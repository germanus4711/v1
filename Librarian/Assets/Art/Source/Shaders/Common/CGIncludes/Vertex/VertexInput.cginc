//------------------------------------------------------------------------------------------
//
//  File Name:         	VertexInput.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	February 28, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_VERTEXINPUT_CGINC )
	#define ECS_VERTEXINPUT_CGINC
		
	struct VertexInput
	{
		float4 vertex : POSITION;
	    half3 normal : NORMAL;

	    #if defined( ECS_VERTEXINPUT_UV1 )
	    	float2 uv1 : TEXCOORD0;
	    #endif
	    
	    #if defined( ECS_VERTEXINPUT_UV2 )
	   		float2 uv2 : TEXCOORD1;
	   	#endif
	   	
	   	#if defined( ECS_VERTEXINPUT_UV3 )
			float2 uv3 : TEXCOORD2;
		#endif

		#if defined( ECS_VERTEXINPUT_UV4 )
			float2 uv4 : TEXCOORD3;
		#endif
		
	   	#if defined( ECS_VERTEXINPUT_TANGENT )
	   		half4 tangent : TANGENT;
	   	#endif
	   	
	   	#if defined( ECS_VERTEXINPUT_VERTEXCOLORS )
	   		fixed4 color : COLOR;
	   	#endif

		#if UNITY_VERSION >= 550
			UNITY_VERTEX_INPUT_INSTANCE_ID
		#endif
	};
#endif