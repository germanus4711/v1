#region Header
// ------------------------------------------------------------------------------------------
// 
//   File Name: Faces.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 22
//   Last Update: 2021 / 11 / 22
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;
using System.Linq;

namespace OOP.Dice
{
    public class Faces : Face
    {
        private List<Face> _faces;

        public bool IsComplete()
        {
            return _faces.All(f => f.HasMinimal());
        }
    }
}