//------------------------------------------------------------------------------------------
//
//  File Name:         	Config.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_CONFIG_CGINC )
	#define ECS_CONFIG_CGINC
	
		//--QUALITY-------------------------------------------------------------------------

			// 0 = Low, 1 = High

			// OpenGL ES 2.0 ( Mobile )
			#if defined( SHADER_API_MOBILE ) && defined( SHADER_API_GLES )
				#define ECS_SHADER_QUALITY 0
			#endif

			// OpenGL ES 2.0 ( WebGL 1.0 )
			#if !defined( SHADER_API_MOBILE ) && defined( SHADER_API_GLES )
				#define ECS_SHADER_QUALITY 1
			#endif

			// OpenGL ES 3.x ( Mobile )
			#if defined( SHADER_API_MOBILE ) && defined( SHADER_API_GLES3 )
				#define ECS_SHADER_QUALITY 1
			#endif

			// OpenGL ES 3.x ( WebGL 2.0 )
			#if !defined( SHADER_API_MOBILE ) && defined( SHADER_API_GLES3 )
				#define ECS_SHADER_QUALITY 1
			#endif

			// Metal
			#if defined( SHADER_API_MOBILE ) && defined( SHADER_API_METAL )
				#define ECS_SHADER_QUALITY 0
			#endif

			// Direct3D 9
			#if defined( SHADER_API_D3D9 )
				#if ( SHADER_TARGET >= 30 )
					#define ECS_SHADER_QUALITY 1
				#else
					#define ECS_SHADER_QUALITY 0
				#endif
			#endif

			// Direct3D 11
			#if defined( SHADER_API_D3D11 )
				#if ( SHADER_TARGET >= 30 )
					#define ECS_SHADER_QUALITY 1
				#else
					#define ECS_SHADER_QUALITY 0
				#endif
			#endif

			// OpenGL
			#if defined( SHADER_API_OPENGL ) || defined( SHADER_API_GLCORE )
				#if ( SHADER_TARGET >= 30 )
					#define ECS_SHADER_QUALITY 1
				#else
					#define ECS_SHADER_QUALITY 0
				#endif
			#endif

			// Fallback to low quality
			#if !defined( ECS_SHADER_QUALITY )
				#define ECS_SHADER_QUALITY 0
			#endif
	
		//--COLORSPACE CORRECTION-----------------------------------------------------------

			#if ( ECS_SHADER_QUALITY == 1 )
				// Convert gamma input to linear
				#define ECS_COLORSPACE_GAMMA2LINEAR_SHADER
	
				// Convert linear output to gamma
				#define ECS_COLORSPACE_LINEAR2GAMMA_SHADER
			#endif
		
		//--DEBUG---------------------------------------------------------------------------

			//#define ECS_DEBUG_DIFFUSE
			//#define ECS_DEBUG_GLOSS
			//#define ECS_DEBUG_NORMALS
			//#define ECS_DEBUG_EMISSION
			//#define ECS_DEBUG_LIGHTING
			//#define ECS_DEBUG_LIGHTING_INDIRECT
			//#define ECS_DEBUG_LIGHTING_INDIRECT_DIFFUSE 
			//#define ECS_DEBUG_LIGHTING_INDIRECT_SPECULAR
			//#define ECS_DEBUG_LIGHTING_DIRECT
			//#define ECS_DEBUG_LIGHTING_DIRECT_DIFFUSE
			//#define ECS_DEBUG_LIGHTING_DIRECT_SPECULAR

		//--CONSTANTS-------------------------------------------------------------------------

			#define ECS_INDIRECTSPECULAR_DIELECTRIC half3( 0.05h, 0.05h, 0.05h )
			#define ECS_INDIRECTSPECULAR_LOD_STEPS 8
#endif