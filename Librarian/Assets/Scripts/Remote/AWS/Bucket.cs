using System;
using System.IO;
using System.Threading.Tasks;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;

namespace Remote.AWS
{
    public class Bucket
    {
        private AmazonS3Client s3Client;
        private CredentialProfile basicProfile;
        private AWSCredentials awsCredentials;

        public Bucket()
        {
            var sharedFile = new SharedCredentialsFile();
            if (sharedFile.TryGetProfile("Librarian", out basicProfile) &&
                AWSCredentialsFactory.TryGetAWSCredentials(basicProfile, sharedFile, out awsCredentials))
                try
                {
                    s3Client = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.Log("Ex = " + e);
                }
            else
                UnityEngine.Debug.Log("Creds wrong!");
            // TODO idk
        }

        // TODO needs testing - probably not for mobile
        public async Task<bool> Create(string bucketName)
        {
            try
            {
                UnityEngine.Debug.Log($"\nCreating bucket {bucketName}...");
                var createResponse = await s3Client.PutBucketAsync(bucketName);
                UnityEngine.Debug.Log($"Result: {createResponse.HttpStatusCode.ToString()}");
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log("Caught exception when creating a bucket:");
                UnityEngine.Debug.Log(e.Message);
            }

            return true;
        }

        public async Task<bool> ListAll()
        {
            UnityEngine.Debug.Log("\nGetting a list of your buckets...");
            var listResponse = await MyListBucketsAsync(s3Client);
            UnityEngine.Debug.Log($"Number of buckets: {listResponse.Buckets.Count}");
            foreach (var b in listResponse.Buckets)
            {
                UnityEngine.Debug.Log(b.BucketName);
                GetObjects(b.BucketName);
            }

            return true;
        }

        public bool GetObjects(string bucketName)
        {
            var objects = s3Client.ListObjects(new ListObjectsRequest() {BucketName = bucketName});
            foreach (var s3Object in objects.S3Objects)
            {
                UnityEngine.Debug.Log($"\t{s3Object.Key} ({s3Object.Size})");
                if (s3Object.Size <= 0) continue;

                // We have a file (not a directory) --> download it
                var bucketData =
                    s3Client.GetObject(new GetObjectRequest() {BucketName = bucketName, Key = s3Object.Key});
                var s3FileName = new FileInfo(s3Object.Key).Name;
                try
                {
                    using var fileStream = File.Create($"Assets/Data/{s3FileName}");
                    UnityEngine.Debug.Log($"File: {fileStream.Name}");
                    bucketData.ResponseStream.CopyTo(fileStream);
                    fileStream.Flush();
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.Log(e);
                }
            }

            return true;
        }

        // Async method to get a list of Amazon S3 buckets.
        private static async Task<ListBucketsResponse> MyListBucketsAsync(IAmazonS3 s3Client)
        {
            return await s3Client.ListBucketsAsync();
        }
    }
}