//------------------------------------------------------------------------------------------
//
//  File Name:         	Blend.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_BLEND ) && !defined( ECS_PASS_SHADOWCASTING )
	#define ECS_INPUT_BLEND

	//--MASK--------------------------------------------------------------------------------

		// Texture
		#if defined( ECS_BLEND_TEXTURE )
				#if !defined( ECS_VERTEXINPUT_UV4 )
					#define ECS_VERTEXINPUT_UV4
				#endif
				
				#if !defined( ECS_VERTEXOUTPUT_UV4 )
					#define ECS_VERTEXOUTPUT_UV4
				#endif
				
			uniform UNITY_DECLARE_TEX2D( _BlendMap );
		// Vertex Color RGBA
		#else
			#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
				#define ECS_VERTEXINPUT_VERTEXCOLORS
			#endif
	
			#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
				#define ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB
			#endif
			
			#if defined( ECS_BLEND_5 )
				#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
					#define ECS_VERTEXOUTPUT_VERTEXCOLORS_A
				#endif
			#endif
		#endif	
		
	//--DIFFUSE-----------------------------------------------------------------------------
		
		// Texture

		#if defined( ECS_DIFFUSEMAP )
			uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _MainTex2 );

			#if defined( ECS_BLEND_3 )
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _MainTex3 );
			#elif defined( ECS_BLEND_4 )
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _MainTex3 );
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _MainTex4 );
			#elif defined( ECS_BLEND_5 )
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _MainTex3 );
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _MainTex4 );
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _MainTex5 );
			#endif

		#endif

		// Color

		uniform half3 _Color2;

		#if defined( ECS_BLEND_3 )
			uniform half3 _Color3;
		#elif defined( ECS_BLEND_4 )
			uniform half3 _Color3;
			uniform half3 _Color4;
		#elif defined( ECS_BLEND_5 )
			uniform half3 _Color3;
			uniform half3 _Color4;
			uniform half3 _Color5;
		#endif

		
	//--NORMALS-----------------------------------------------------------------------------
		
		// Texture
		#if defined( ECS_NORMALMAP )
			uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _BumpMap2 );
			#if defined( ECS_BLEND_3 )
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _BumpMap3 );
			#elif defined( ECS_BLEND_4 )
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _BumpMap3 );
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _BumpMap4 );
			#elif defined( ECS_BLEND_5 )
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _BumpMap3 );
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _BumpMap4 );
				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _BumpMap5 );
			#endif
		#endif

	//--UV----------------------------------------------------------------------------------

		#if defined( ECS_DIFFUSEMAP ) || defined( ECS_NORMALMAP )
			uniform float4 _BlendTiling;
		#endif

	//--THRESHOLD---------------------------------------------------------------------------	
		uniform fixed4 _BlendMinThreshold;
		uniform fixed4 _BlendMaxThreshold;
#endif