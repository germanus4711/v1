﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Remote.DB.Database.data
{
    public class V1_Blacklist
    {
        private List<V1User> _users;
        private V1_Database _db;

        public List<V1User> Users
        {
            get => _users;
            set => _users = value;
        }

        public V1_Blacklist()
        {
            _db = new V1_Database();
            _users = new List<V1User>();
        }

        public void FillList()
        {
            _db.Open();
            var cmd = new MySqlCommand("SELECT userId FROM `game`.blacklist", _db.DBConnection);
            var dataReader = cmd.ExecuteReader();
            _users.Clear();
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                {
                    V1User user = new V1User
                    {
                        UserId = dataReader.GetString(dataReader.GetOrdinal("userId"))
                    };
                    _users.Add(user);
                }
            }

            dataReader.Close();

            cmd.Dispose();

            _db.Close();
        
            if (!_db.Close())
            {
                UnityEngine.Debug.LogError("Database connection could not be closed!");
            }
        }

        public void Save(V1User user)
        {
            _db.Open();
            var cmd = new MySqlCommand("INSERT INTO game.blacklist ( userId ) VALUES (@userId)", _db.DBConnection);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@userId", user.UserId); 
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            _db.Close();
        }

        public void Remove(V1User user)
        {
            _db.Open();
            var cmd = new MySqlCommand("DELETE FROM game.blacklist WHERE userId = @userId", _db.DBConnection);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@userId", user.UserId);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            _db.Close();
        }
    }
}
