//------------------------------------------------------------------------------------------
//
//  File Name:         	VertexProgram.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	February 28, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( VertexProgram_CGINC )
	#define VertexProgram_CGINC

	// Includes
	#include "../Common/CGIncludes/Vertex/VertexInput.cginc"
	#include "../Common/CGIncludes/Vertex/VertexOutput.cginc"

	VertexOutput VertexProgram( VertexInput v )
	{
		#if UNITY_VERSION >= 550
			UNITY_SETUP_INSTANCE_ID( v );
		#endif	

		VertexOutput vertexOutput;

		UNITY_INITIALIZE_OUTPUT( VertexOutput, vertexOutput );

		#if UNITY_VERSION >= 550
			UNITY_TRANSFER_INSTANCE_ID( v, vertexOutput );
			UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( vertexOutput );
		#endif	
		
		//--POSITION------------------------------------------------------------------------
		
			// Object Space
			#if defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 )
				v.vertex.xyz = ECS_VertexWind( v.vertex.xyz, _WindStrength, _DirectionalWindStrength, v.color.a );
			#endif

			#if UNITY_VERSION >= 550
				vertexOutput.pos = UnityObjectToClipPos( v.vertex );
			#else
				vertexOutput.pos = mul( UNITY_MATRIX_MVP, v.vertex );
			#endif
				
			// World Space
			#if UNITY_VERSION >= 550
				float4 posWorld = mul( unity_ObjectToWorld, v.vertex );	
			#else
				float4 posWorld = mul( _Object2World, v.vertex );
			#endif

			#if defined( ECS_VERTEXOUTPUT_WORLDPOSITION )
				vertexOutput.posWorld = posWorld.xyz;
			#endif	
				
			// Screen Space
			#if defined( ECS_VERTEXOUTPUT_SCREENPOSITION )
				vertexOutput.posScreen = ComputeScreenPos( vertexOutput.pos );
			#endif
				
		//--UV COORDINATES------------------------------------------------------------------
				
			//--UV1/UV2

				#if defined( ECS_VERTEXINPUT_UV1 ) && defined( ECS_INPUT_UV1_TRANSFORM )
					v.uv1.xy = v.uv1.xy * _UV1.x + _UV1.yz * _Time.y;
				#endif
	
				#if defined( ECS_VERTEXINPUT_UV2 ) && defined( ECS_INPUT_UV2_TRANSFORM )
					v.uv2.xy = v.uv2.xy * _UV2.x + _UV2.yz * _Time.y;
				#endif
	
				#if defined( ECS_VERTEXOUTPUT_UV1 )
					vertexOutput.UV12.xy = v.uv1.xy;
					#if defined( ECS_VERTEXOUTPUT_UV2 )
						#if defined( ECS_UNITY_LIGHTMAP )
							vertexOutput.UV12.zw = v.uv2.xy * unity_LightmapST.xy + unity_LightmapST.zw;
						#else
							vertexOutput.UV12.zw = v.uv2.xy;
						#endif
					#endif
				#elif defined( ECS_VERTEXOUTPUT_UV2 )
					#if defined( ECS_UNITY_LIGHTMAP )
						vertexOutput.UV12.xy = v.uv2.xy * unity_LightmapST.xy + unity_LightmapST.zw;
					#else
						vertexOutput.UV12.xy = v.uv2.xy;
					#endif
				#endif
		
			//--UV3/UV4

				#if defined( ECS_VERTEXINPUT_UV3 ) && defined( ECS_INPUT_UV3_TRANSFORM )
					v.uv3.xy = v.uv3.xy * _UV3.x + _UV3.yz * _Time.y;
				#endif
	
				#if defined( ECS_VERTEXINPUT_UV4 ) && defined( ECS_INPUT_UV4_TRANSFORM )
					v.uv4.xy = v.uv4.xy * _UV4.x + _UV4.yz * _Time.y;
				#endif
	
				#if defined( ECS_VERTEXOUTPUT_UV3 )
					#if defined( ECS_UNITY_DYNAMICLIGHTMAP )
						vertexOutput.UV34.xy = v.uv3.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
					#else
						vertexOutput.UV34.xy = v.uv3.xy;
					#endif
						
					#if defined( ECS_VERTEXOUTPUT_UV4 )
						vertexOutput.UV34.zw = v.uv4.xy;
					#endif
				#elif defined( ECS_VERTEXOUTPUT_UV4 )
					vertexOutput.UV34.xy = v.uv4.xy;
				#endif				
					
		//--NORMALS-------------------------------------------------------------------------
		
			float3 normalWorld = UnityObjectToWorldNormal( v.normal );
			
			#if defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
				float4 tangentWorld = float4( UnityObjectToWorldDir( v.tangent.xyz ), v.tangent.w );
				float3x3 tangentToWorld = ECS_CreateTangentToWorldPerVertex( normalWorld, tangentWorld.xyz, tangentWorld.w );
				vertexOutput.tangentToWorld1.xyz = tangentToWorld[0];
				vertexOutput.tangentToWorld2.xyz = tangentToWorld[1];
				vertexOutput.tangentToWorld3.xyz = tangentToWorld[2];
			#else
				vertexOutput.normalWorld = normalWorld;
			#endif

		//--VERTEX COLORS
		
			#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
				// Colorspace Correction
				#if defined( ECS_VERTEXINPUT_VERTEXCOLORS_GAMMA ) && defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
					v.color.rgb = ECS_GammaToLinear( v.color.rgb );
				#endif

				#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
					vertexOutput.color = v.color.rgba;
				#else
					vertexOutput.color = v.color.rgb;
				#endif
			#else
				#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
					vertexOutput.color = v.color.a;
				#endif
			#endif
			
		//--LIGHTING

			//--INDIRECT

				#if defined( ECS_VERTEXOUTPUT_INDIRECTDIFFUSE ) && defined( ECS_UNITY_SH )
					half3 indirectDiffuse = 0;

					// Quality : High
					#if ( ECS_SHADER_QUALITY == 1 ) && defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
						indirectDiffuse = SHEvalLinearL2( half4( normalWorld, 1.0h ) );
					// Quality : Low (Fallback)
					#else
						#if defined( ECS_VERTEX_RGB_INDIRECTDIFFUSE )
							indirectDiffuse = v.color.rgb;
						#else
							indirectDiffuse = _IndirectDiffuseColor;
	
							// Colorspace Correction
							#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
								if ( ECS_IsGammaSpace( ) )
									indirectDiffuse = ECS_GammaToLinear( indirectDiffuse );
							#endif
						#endif
						indirectDiffuse *= SHEvalLinearL0L1( half4( normalWorld, 1.0h ) ) + SHEvalLinearL2( half4( normalWorld, 1.0h ) );
					#endif

					// Approximated illumination from non-important point lights
					// Ideally this should be colorspace corrected
					#if defined( ECS_INPUT_LIGHTING_DIRECT ) && defined( VERTEXLIGHT_ON ) && defined( ECS_PASS_FORWARDBASE )
						indirectDiffuse += Shade4PointLights ( unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0, unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb, unity_4LightAtten0, posWorld, normalWorld );
					#endif

					#if defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
						vertexOutput.tangentToWorld1.w = indirectDiffuse.r;
						vertexOutput.tangentToWorld2.w = indirectDiffuse.g;
						vertexOutput.tangentToWorld3.w = indirectDiffuse.b;
					#else
						vertexOutput.indirectDiffuse = indirectDiffuse;
					#endif
				#endif

			//--DIRECT
			
				// Forward Base Lighting
				#if defined( ECS_INPUT_LIGHTING_DIRECT ) && defined( ECS_PASS_FORWARDBASE )
					// Light Direction - World Space
					vertexOutput.lightDirection.xyz = ECS_NormalizePerVertex( _WorldSpaceLightPos0.xyz - posWorld.xyz * _WorldSpaceLightPos0.w );

					// Shadow Receiving
					#if !defined( ECS_ALPHABLEND ) && !defined( ECS_ALPHAFADE )
						TRANSFER_SHADOW( vertexOutput );
					#endif
				#endif
				
				// Forward Add Pass Lighting
				#if defined( ECS_INPUT_LIGHTING_DIRECT ) && defined( ECS_PASS_FORWARDADD )
					// Light Direction - World Space
					// When normal mapping stored with tangent to world matrix

					#if defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
						half3 lightDirection = ECS_NormalizePerVertex( _WorldSpaceLightPos0.xyz - posWorld.xyz * _WorldSpaceLightPos0.w );
						vertexOutput.tangentToWorld1.w = lightDirection.x;
						vertexOutput.tangentToWorld2.w = lightDirection.y;
						vertexOutput.tangentToWorld3.w = lightDirection.z;
					#else
						vertexOutput.lightDirection.xyz = ECS_NormalizePerVertex( _WorldSpaceLightPos0.xyz - posWorld.xyz * _WorldSpaceLightPos0.w );
					#endif

					// Shadow Receiving
					#if !defined( ECS_ALPHABLEND ) && !defined( ECS_ALPHAFADE )
						TRANSFER_VERTEX_TO_FRAGMENT( vertexOutput );
					#endif
				#endif
			
		//--FOG/EYE DIRECTION
		
			// World Space Eye Direction
			#if defined( ECS_INPUT_LIGHTING_DIRECT ) || defined( ECS_INPUT_LIGHTING_INDIRECT )
				#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD )
					vertexOutput.fogCoord.yzw = ECS_NormalizePerVertex( posWorld.xyz - _WorldSpaceCameraPos );
				#elif defined( ECS_PASS_DEFERRED )
					vertexOutput.eyeDirection = ECS_NormalizePerVertex( posWorld.xyz - _WorldSpaceCameraPos );
				#endif
			#endif
		
			#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD )
				UNITY_TRANSFER_FOG( vertexOutput,vertexOutput.pos );
			#endif

		return vertexOutput;
	}
#endif