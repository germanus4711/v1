using UnityEngine;

namespace Login
{
    public static class Config
    {
        public static Font Font => (Font) Resources.Load("ARIALN");
        public static int FontSize => 16;
    }
}