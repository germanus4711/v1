//------------------------------------------------------------------------------------------
//
//  File Name:         	IndirectDiffuse.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_LIGHTING_INDIRECT_DIFFUSE_CGINC ) && defined( ECS_INPUT_LIGHTING_INDIRECT )
	#define ECS_LIGHTING_INDIRECT_DIFFUSE_CGINC

	// UnityGlobalIllumination.cginc, 5.3.4
	inline half3 MixLightmapWithRealtimeAttenuation ( half3 lightmapContribution, half attenuation, fixed4 bakedColorTex )
	{
		// Let's try to make realtime shadows work on a surface, which already contains
		// baked lighting and shadowing from the current light.
		// Generally do min( lightmap,shadow ), with "shadow" taking overall lightmap tint into account.
		half3 shadowLightmapColor = bakedColorTex.rgb * attenuation;
		half3 darkerColor = min( lightmapContribution, shadowLightmapColor );
	
		// However this can darken overbright lightmaps, since "shadow color" will
		// never be overbright. So take a max of that color with attenuated lightmap color.
		return max( darkerColor, lightmapContribution * attenuation );
	}

	inline half3 ECS_DecodeDirectionalLightmap ( half3 indirectDiffuse, fixed3 lightmapDirectional, half3 normalTangent )
	{
		half halfLambert = dot( normalTangent, lightmapDirectional - 0.5 ) + 0.5h;
		return indirectDiffuse * halfLambert / lightmapDirectional.z;
	}

	inline half3 ECS_Lighting_Indirect_Diffuse( half3 indirectDiffuse, VertexOutput vertexOutput, Fragment fragment )
	{
		//--LIGHTMAPS

			// ECS Baked
			#if defined( ECS_LIGHTMAP ) || defined( ECS_DIRLIGHTMAP )

				// Non-Directional
				indirectDiffuse = fragment.lightmap;

				// Directional
				#if defined( ECS_DIRLIGHTMAP ) && ( ECS_SHADER_QUALITY == 1 ) && defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
					indirectDiffuse = ECS_DecodeDirectionalLightmap( indirectDiffuse, fragment.lightmapDirectional, fragment.normalTangent );
				#endif
					
			// Unity Baked
			#elif defined( ECS_UNITY_LIGHTMAP )

				// Non-Directional
				#if defined( ECS_VERTEXOUTPUT_UV1 )
					fixed4 lightmap = UNITY_SAMPLE_TEX2D( unity_Lightmap, vertexOutput.UV12.zw );
				#else
					fixed4 lightmap = UNITY_SAMPLE_TEX2D( unity_Lightmap, vertexOutput.UV12.xy );
				#endif
				
				indirectDiffuse = DecodeLightmap( lightmap );

				// Colorspace Correction
				#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
					if ( ECS_IsGammaSpace(  ) )
						indirectDiffuse = ECS_GammaToLinear( indirectDiffuse );
				#endif

				#if defined( DIRLIGHTMAP_COMBINED ) 
					#if defined( ECS_VERTEXOUTPUT_UV1 )
						fixed4 lightmapDirectional = UNITY_SAMPLE_TEX2D_SAMPLER( unity_LightmapInd, unity_Lightmap, vertexOutput.UV12.zw );
					#else
						fixed4 lightmapDirectional = UNITY_SAMPLE_TEX2D_SAMPLER( unity_LightmapInd, unity_Lightmap, vertexOutput.UV12.xy );
					#endif

					indirectDiffuse = DecodeDirectionalLightmap( indirectDiffuse, lightmapDirectional, fragment.normalWorld );
				#endif

				#if defined( SHADOWS_SCREEN ) && !( defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) )
					indirectDiffuse = MixLightmapWithRealtimeAttenuation( indirectDiffuse, SHADOW_ATTENUATION( vertexOutput ), lightmap );
				#endif
			#endif

			// Unity Dynamic
			#if defined( ECS_UNITY_DYNAMICLIGHTMAP )

				// Non-Directional
				fixed4 lightmapDynamic = UNITY_SAMPLE_TEX2D( unity_DynamicLightmap, vertexOutput.UV34.xy );
				half3 indirectDiffuseDynamic = DecodeRealtimeLightmap( lightmapDynamic );

				#if defined( DIRLIGHTMAP_OFF )
					indirectDiffuse += indirectDiffuseDynamic;

				// Directional
				#elif defined( DIRLIGHTMAP_COMBINED )
					half4 lightmapDynamicDirectional = UNITY_SAMPLE_TEX2D_SAMPLER( unity_DynamicDirectionality, unity_DynamicLightmap, vertexOutput.UV34.xy );
					indirectDiffuse += DecodeDirectionalLightmap( indirectDiffuseDynamic, lightmapDynamicDirectional, fragment.normalWorld );
				#endif
			#endif
				
		//--UNITY SPHERICAL HARMONICS
		// If we don't have a lightmap we sample Unity's spherical harmonics ( Ambient Source/Light Probes )

			#if defined( ECS_UNITY_SH ) && defined( ECS_VERTEXOUTPUT_INDIRECTDIFFUSE )
				#if defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
					// Quality : High
					#if ( ECS_SHADER_QUALITY == 1 )
						#if defined( ECS_VERTEX_RGB_INDIRECTDIFFUSE )
							indirectDiffuse = vertexOutput.color.rgb;
						#else
							indirectDiffuse = _IndirectDiffuseColor;

							// Colorspace Correction
							#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
								if ( ECS_IsGammaSpace(  ) )
									indirectDiffuse = ECS_GammaToLinear( indirectDiffuse );
							#endif
						#endif

						//#if UNITY_LIGHT_PROBE_PROXY_VOLUME
							//if ( unity_ProbeVolumeParams.x == 1.0 )
								//indirectDiffuse *= half3( vertexOutput.tangentToWorld1.w, vertexOutput.tangentToWorld2.w, vertexOutput.tangentToWorld3.w ) + SHEvalLinearL0L1_SampleProbeVolume ( half4( fragment.normalWorld, 1.0 ), vertexOutput.posWorld );
							//else
								//indirectDiffuse *= half3( vertexOutput.tangentToWorld1.w, vertexOutput.tangentToWorld2.w, vertexOutput.tangentToWorld3.w ) + SHEvalLinearL0L1 ( half4( fragment.normalWorld, 1.0 ) );
						//#else
							indirectDiffuse *=half3( vertexOutput.tangentToWorld1.w, vertexOutput.tangentToWorld2.w, vertexOutput.tangentToWorld3.w ) + SHEvalLinearL0L1( half4( fragment.normalWorld, 1.0h ) );
						//#endif

						indirectDiffuse = max( half3( 0, 0, 0 ), indirectDiffuse );
					// Quality : Low ( Fallback )
					#else
						indirectDiffuse = half3( vertexOutput.tangentToWorld1.w, vertexOutput.tangentToWorld2.w, vertexOutput.tangentToWorld3.w );
					#endif
				#else
					indirectDiffuse = vertexOutput.indirectDiffuse;
				#endif
			#endif

		// Occlusion		
			#if defined( ECS_INPUT_LIGHTING_OCCLUSION )
				indirectDiffuse *= fragment.occlusion.b;
			#endif

		//--OUTPUT

			// Debug
			#if defined( ECS_DEBUG_LIGHTING_INDIRECT_DIFFUSE ) || defined( ECS_DEBUG_LIGHTING_INDIRECT ) || defined( ECS_DEBUG_LIGHTING )
				return indirectDiffuse;
			// Normal
			#else
				return indirectDiffuse * fragment.diffuse;
			#endif
	}
#endif