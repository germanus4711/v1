using MySql.Data.MySqlClient;
using static Remote.DB.Database.V1_Globals;

namespace Remote.DB.Database
{
    public class V1_Database
    {
        private string _dbURL = DBSERVER;
        private string _dbSchema = "";
        private string _dbPass = DBPASS;
        private string _dbUser = DBUSER;
        private MySqlConnection _dbConnection;

        public V1_Database()
        {
        }

        public V1_Database(string dbURL, string dbSchema, string dbUser, string dbPass)
        {
            _dbURL = dbURL;
            _dbSchema = dbSchema;
            _dbPass = dbPass;
            _dbUser = dbUser;
            _dbConnection = null;
            UnityEngine.Debug.Log($"db = {dbURL}");
        }

        public string Dburl
        {
            get => _dbURL;
            set => _dbURL = value;
        }

        public string DBSchema
        {
            get => _dbSchema;
            set => _dbSchema = value;
        }

        public string DBPass
        {
            get => _dbPass;
            set => _dbPass = value;
        }

        public string DBUser
        {
            get => _dbUser;
            set => _dbUser = value;
        }

        public MySqlConnection DBConnection
        {
            get => _dbConnection;
            set => _dbConnection = value;
        }

        public string GetConnectionString()
        {
            return "server=" + _dbURL + ";" + "port=3306;" + "database=" +
                   _dbSchema + ";" + "user=" + _dbUser + ";" + "password=" + _dbPass + ";";
        }

        public bool Open()
        {
            var retVal = false;
            if (_dbConnection == null)
            {
                var connectionString = GetConnectionString();
                var retries = DBRETRIES;
                while (retries > 0)
                {
                    try
                    {
                        _dbConnection = new MySqlConnection(connectionString);
                        // Debug.Log($"dbstr = {connectionString}");
                        _dbConnection.Open();
                        retVal = true;
                        UnityEngine.Debug.Log($"{DBSchema} is connected, state = {_dbConnection.State.ToString()}");
                        retries = 0;
                    }
                    catch (MySqlException ex)
                    {
                        switch (ex.Number)
                        {
                            case 0:
                                UnityEngine.Debug.Log(
                                    $"({DBSchema}) [{retries}] Cannot connect to server. {connectionString}");
                                break;
                            case 1045:
                                UnityEngine.Debug.Log(
                                    $"({DBSchema}) [{retries}] Invalid username/password. {connectionString}");
                                break;
                            default:
                                UnityEngine.Debug.Log($"({DBSchema}) [{retries}] exception #{ex.Number} {ex.Message}");
                                break;
                        }
                    }

                    retries--;
                }
            }
            else
            {
                retVal = true;
            }

            return retVal;
        }

        public bool IsOpen()
        {
            return _dbConnection != null;
        }

        public bool IsClosed()
        {
            return !IsOpen();
        }

        public bool Close()
        {
            if (_dbConnection == null) return IsClosed();

            _dbConnection.Close();
            _dbConnection = null;
            UnityEngine.Debug.Log(DBSchema + "is closed!");

            return IsClosed();
        }
    }
}