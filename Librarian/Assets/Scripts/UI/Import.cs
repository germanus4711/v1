using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace UI
{
    public class Import
    {
        public List<string> items;
        public int Count = 0;

        public Import()
        {
            items = new List<string>();
        }

        public void Load()
        {
            const string fileName = "Menu.txt";

            try
            {
                // #if UNITY_EDITOR || UNITY_EDITOR_OSX
                // using var fileStream = File.OpenRead($"Assets/Resources/{fileName}");
                // #elif UNITY_IOS
                using var fileStream = File.OpenRead(Application.dataPath + $"/StreamingAssets/{fileName}");
                // using var fileStream = File.OpenRead(Application.dataPath + $"/Raw/{fileName}");
                // #endif
                Debug.Log($"File: {fileStream.Name}");
                Remote.Debug.Remote.Log($"File: {fileStream.Name}");
                var reader = new StreamReader(fileStream);
                items = reader.ReadToEnd().Split(new[] {Path.DirectorySeparatorChar}, StringSplitOptions.None).ToList();
                Count = items.Count;
            }
            catch (Exception e)
            {
                Debug.Log(e);
                Remote.Debug.Remote.Log(e.ToString());
            }
        }
    }
}