//------------------------------------------------------------------------------------------
//
//  File Name:         	ForwardAddBypass.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	February 28, 2019
//  Last Update:    	February 28, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined(ECS_PASS_FORWARDADDBYPASS_CGINC)
	#define ECS_PASS_FORWARDADDBYPASS_CGINC

	// Unity Includes
	#include "UnityCG.cginc"

	// Vertex Program Input		
	struct VertexInput
	{
		float4 vertex : POSITION;

		#if UNITY_VERSION >= 550
			UNITY_VERTEX_INPUT_INSTANCE_ID
		#endif
	};

	// Vertex Program Output
	struct VertexOutput
	{
		float4 pos : SV_POSITION;

		#if UNITY_VERSION >= 550
			UNITY_VERTEX_OUTPUT_STEREO
			UNITY_VERTEX_INPUT_INSTANCE_ID
		#endif
	};

	// Vertex Program
	VertexOutput VertexProgram( VertexInput v )
	{
		VertexOutput vertexOutput;

		UNITY_INITIALIZE_OUTPUT( VertexOutput, vertexOutput );

		#if UNITY_VERSION >= 550
			UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( vertexOutput );
			UNITY_SETUP_INSTANCE_ID(v);
			UNITY_TRANSFER_INSTANCE_ID(v, vertexOutput);
		#endif	

		// Object Space Position
		#if defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 )
			v.vertex.xyz = ECS_VertexWind( v.vertex.xyz, _WindStrength, _DirectionalWindStrength, v.color.a );
		#endif

		#if UNITY_VERSION >= 550
			vertexOutput.pos = UnityObjectToClipPos( v.vertex );
		#else
			vertexOutput.pos = mul( UNITY_MATRIX_MVP, v.vertex );
		#endif
				
		return vertexOutput;
	}

	// Fragment Program
	half4 FragmentProgram( VertexOutput vertexOutput ) : SV_Target 
	{
		return half4(0, 0, 0, 0);
	}

#endif