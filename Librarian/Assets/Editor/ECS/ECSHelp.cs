using System.Diagnostics.CodeAnalysis;
using UnityEditor;
using UnityEngine;

namespace Editor.ECS
{
    [SuppressMessage("ReSharper", "All")]
    public class ECSHelp : EditorWindow
    {
        [MenuItem("Help/ECS IT")]
        private static void itHelp()
        {
            Application.OpenURL("https://ecssupport.atlassian.net/servicedesk/customer/portal/1");
        }

        [MenuItem("Office/Timecard")]
        private static void Timecard()
        {
            Application.OpenURL("https://ecsorl-cp.costpointfoundations.com/cpweb/masterPage.htm");
        }

        [MenuItem("Office/Jenkins")]
        private static void Jenkins()
        {
            Application.OpenURL("https://jenkins.ecsorl.com");
        }
        
        [MenuItem("Office/Jira")]
        private static void Jira()
        {
            Application.OpenURL("https://jira.ecsorl.com");
        }
        
    }
}