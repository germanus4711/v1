//------------------------------------------------------------------------------------------
//
//  File Name:         	Diffuse.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	February 6, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_DIFFUSE ) && ( !defined( ECS_PASS_SHADOWCASTING ) || ( defined( ECS_PASS_SHADOWCASTING ) && ( defined( ECS_ALPHATEST ) || defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) ) ) )
	#define ECS_INPUT_DIFFUSE
	
	//--DIFFUSE-----------------------------------------------------------------------------

		uniform half3 _Color;

		// Texture
		#if defined( ECS_DIFFUSEMAP ) 
			#if !defined( ECS_VERTEXINPUT_UV1 )
				#define ECS_VERTEXINPUT_UV1
			#endif
			
			#if !defined( ECS_VERTEXOUTPUT_UV1 )
				#define ECS_VERTEXOUTPUT_UV1
			#endif
			
			uniform UNITY_DECLARE_TEX2D( _MainTex );

			#if defined( ECS_SHADER_LEGACY )
				uniform float _DiffuseHasGloss;
			#endif
		// Vertex Color RGB
		#elif defined( ECS_VERTEX_RGB_DIFFUSE )
			#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
				#define ECS_VERTEXINPUT_VERTEXCOLORS
			#endif

			#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
				#define ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB
			#endif

			#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS_GAMMA )
				#define ECS_VERTEXINPUT_VERTEXCOLORS_GAMMA
			#endif
		#endif
		
	//--TRANSPARENCY------------------------------------------------------------------------
	
		#if defined( ECS_ALPHATEST ) || defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
			uniform half _Cutoff;

			#if ( ECS_SHADER_QUALITY == 1 ) && ( defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED ) )
				uniform UNITY_DECLARE_TEX2D( _CutoffFalloffRamp );
			#endif

			// Vertex Color Alpha
			#if defined( ECS_VERTEX_A_ALPHA )
				#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
					#define ECS_VERTEXINPUT_VERTEXCOLORS
				#endif
				#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
					#define ECS_VERTEXOUTPUT_VERTEXCOLORS_A
				#endif
			#endif
		#endif
#endif