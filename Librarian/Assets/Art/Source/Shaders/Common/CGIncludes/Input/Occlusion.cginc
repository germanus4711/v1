//------------------------------------------------------------------------------------------
//
//  File Name:         	Occlusion.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_LIGHTING_OCCLUSION ) && ( defined( ECS_INPUT_LIGHTING_DIRECT ) || defined( ECS_INPUT_LIGHTING_INDIRECT ) )

	// Texture
	#if defined( ECS_OCCLUSIONMAP ) && !defined( ECS_DIRLIGHTMAP )
		#if !defined( ECS_VERTEXINPUT_UV2 )
			#define ECS_VERTEXINPUT_UV2
		#endif
			
		#if !defined( ECS_VERTEXOUTPUT_UV2 )
			#define ECS_VERTEXOUTPUT_UV2
		#endif

		uniform UNITY_DECLARE_TEX2D( _OcclusionMap );

		#if !defined( ECS_INPUT_LIGHTING_OCCLUSION )
			#define ECS_INPUT_LIGHTING_OCCLUSION
			uniform fixed4 _OcclusionStrength;
		#endif
	#endif
	
	// Vertex Color : Alpha
	#if defined( ECS_VERTEX_A_OCCLUSION )
		#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
			#define ECS_VERTEXINPUT_VERTEXCOLORS
		#endif
			
		#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
			#define ECS_VERTEXOUTPUT_VERTEXCOLORS_A
		#endif

		#if !defined( ECS_INPUT_LIGHTING_OCCLUSION )
			#define ECS_INPUT_LIGHTING_OCCLUSION
			uniform fixed4 _OcclusionStrength;
		#endif
	#endif
#endif