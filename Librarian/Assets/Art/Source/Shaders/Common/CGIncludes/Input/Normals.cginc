//------------------------------------------------------------------------------------------
//
//  File Name:         	Normals.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_NORMAL ) && !defined( ECS_PASS_SHADOWCASTING )
	#define ECS_INPUT_NORMAL
	
	// Texture
	#if defined( ECS_NORMALMAP )
		#if !defined( ECS_VERTEXINPUT_UV1 )
			#define ECS_VERTEXINPUT_UV1
		#endif
	
		#if !defined( ECS_VERTEXOUTPUT_UV1 )
			#define ECS_VERTEXOUTPUT_UV1
		#endif

		#if !defined( ECS_VERTEXINPUT_TANGENT )
			#define ECS_VERTEXINPUT_TANGENT
		#endif
	
		#if !defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
			#define ECS_VERTEXOUTPUT_TANGENTTOWORLD
		#endif
			
		UNITY_DECLARE_TEX2D( _BumpMap );

		#if ( ECS_SHADER_QUALITY == 1 )
			uniform half _BumpStrength;
		#endif
	#endif
#endif