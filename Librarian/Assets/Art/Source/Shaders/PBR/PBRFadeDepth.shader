﻿//------------------------------------------------------------------------------------------
//
//  File Name:         	PBRFadeDepth.shader
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	June 7, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

Shader "Hidden/ECS/PBR/FadeDepth"
{
	// Properties

	Properties
	{
		// Render Settings

		_RenderMode( "Render Mode", Float ) = 0.0 // 0 = Opaque, 1 = Cutout, 2 = Transparent, 3 = Fade, 4 = FadeDepth, 5 = Decal
		_RenderQueue( "Render Queue", Float ) = 2000.0 // Opaque = 2000, Cutout = 2600, Transparent = 2999, Fade = 2999, Decal = 2020
		_CullMode( "Cull Mode", Float ) = 2.0 // 0 = Off, 1 = Front, 2 = Back
		_ZWrite( "ZWrite", Float ) = 1.0 // 0 = Off, 1 = On
		_ZTest( "ZTest", Float ) = 4.0 // 0 = Disabled, 1 = Never, 2 = Less, 3 = Equal, 4 = LessEqual, 5 = Greater, 6 = Not Equal, 7 = Greater Equal, 8 = Always

		// Diffuse

		_Color( "Diffuse Color", Color ) = ( 1.0, 1.0, 1.0, 1.0 )
		[NoScaleOffset] _MainTex( "Diffuse (RGB)", 2D ) = "white" {}

		// Transparency

		_Cutoff( "Transparency", Range( 0.0, 1.0 ) ) = 1.0
		[NoScaleOffset] _CutoffFalloffRamp( "Transparency Falloff Ramp (R)", 2D ) = "white" {}
				
		// Surface Properties
		// Metalness : Defines the conductivity of a surface (dielecctric/conductor). Black = Dielectric, White = Conductor.
		// Roughness : Defines the roughness of the surface. Black = Smooth, White = Rough.
		// Micro Occlusion : Defines texture level ambient occlusion of the surface. White = No Occlusion, Black = Complete Occlusion.
			
		_Metalness ( "Metalness", Range( 0.0, 1.0 ) ) = 0.0
		_Roughness ( "Roughness", Range( 0.0, 1.0 ) ) = 0.5
		_SurfaceMap ( "Metalness (R), Roughness (G), Micro Occlusion (B)", 2D ) = "white" {}

		// Normals ( Tangent Space )

		[Normal][NoScaleOffset] _BumpMap( "Normal Map", 2D ) = "bump" {}
		_BumpStrength( "Normal Map Strength", Range( 0.0, 3.0 ) ) = 1.0

		// Detail
		// Red Channel : Multiply 2x with diffuse. Black = Darken, Middle Grey = No Change, White = Lighten
		// Green Channel : Multiply 2x, Add/Subtract with roughness. Black = Add Smoothness, Grey = No Change, White = Add Roughness.
		// Blue Channel : Multiply 2x, Add/Subtract with metalness. 
		// Alpha Channel : Multiply 1x with occlusion

		[NoScaleOffset]_DetailMap ( "Detail (R), Roughness (G), Metalness (B), Micro Occlusion (A)", 2D ) = "gray" {}
		_DetailTiling("Detail Map Tiling", Float) = 1.0
		_DetailStrength( "Detail Map Strength", Range( 0.0, 5.0 ) ) = 1.0
		_DetailRoughnessStrength( "Detail Roughness Strength", Range( 0.0, 5.0 ) ) = 1.0

		[NoScaleOffset]_Detail2Map( "Detail (R), Roughness (G), Metalness (B), Micro Occlusion (A)", 2D ) = "gray" {}
		_Detail2Tiling( "Detail Map Tiling", Float) = 1.0
		_Detail2Strength( "Detail Map Strength", Range( 0.0, 20.0 ) ) = 1.0
		_Detail2RoughnessStrength( "Detail Roughness Strength", Range( 0.0, 5.0 ) ) = 1.0

		[Normal][NoScaleOffset] _DetailBumpMap( "Detail Normal Map", 2D ) = "bump" {}
		_DetailBumpStrength( "Detail Normal Map Strength", Range( 0.0, 3.0 ) ) = 1.0

		_DetailUV2( "Use UV2 For Detail?", Float ) = 0 // 0 = No, 1 = Yes
		[NoScaleOffset]_DetailMaskMap( "Detail Mask (R)", 2D ) = "white" {}

		// Emission

		[PerRendererData] _HighlightColor( "Highlight Color", Color ) = ( 0.0, 0.0, 0.0, 0.0 )
		[PerRendererData] _HighlightRamp( "Highlight Ramp (R)", 2D ) = "white" {}
		_EmissionStrength( "Emission Strength", Float ) = 0
		_EmissionColor( "Emission Color", Color ) = ( 1.0, 1.0, 1.0, 1.0 )
		[NoScaleOffset] _EmissionMap( "Emission Map (RGB)", 2D ) = "black" {}

		// Occlusion

		[NoScaleOffset] _OcclusionMap( "Occlusion Map", 2D ) = "white" {}
		_OcclusionStrength( "Occlusion Strength", Vector ) = ( 0.5, 0.5, 1.0, 4.0 )

		// Direct Lighting

		_DirectLighting( "Direct Lighting?", Float ) = 1 // 0 = No, 1 = Yes

		// Indirect Diffuse Lighting

		_IndirectDiffuseMode( "Indirect Diffuse Mode", Float ) = 0.0 // 0 = Color Picker, 1 = Lightmap, 2 = Vertex Color RGB
		[HDR] _LightingMap( "Lighting Map", 2D ) = "black" {}
		_DirLightingMap( "Directional Lighting Map (RG), Occlusion Map (B)", 2D ) = "gray" {}
		_IndirectDiffuseColor( "Indirect Diffuse Color", Color ) = ( 1.0, 1.0, 1.0, 1.0 )

		// Indirect Specular Lighting

		_IndirectSpecularMode( "Indirect Specular Mode", Float ) = 0 // 0 = None, 1 = Custom Cubemap, 2 = Planar Reflection, 3 = Probe
		_IndirectSpecularCube( "Custom Indirect Specular Cubemap", Cube ) = "black" {}
		_PlanarReflection( "Planar Reflection", 2D ) = "black" {}

		// Vertex Colors
				
		_VertexRGBMode( "Vertex Color RGB Mode", Float ) = 0.0 // 0 = Off, 1 = Diffuse Color, 2 = Emission Color, 3 = Indirect Diffuse Color
		_VertexAMode( "Vertex Color RGB Mode", Float ) = 0.0 // 0 = Off, 1 = Transparency, 2 = Emission Mask, 3 = Detail Mask, 4 = Occlusion, 5 = Wind Mask
				
		// UV Coordinates

		_UV1( "UV1 - Tiling (X), X Scoll (Y), Y Scroll (Z)", Vector ) = ( 1.0, 0.0, 0.0, 0.0 )
	}

	// Shader Model 3.0

	SubShader
	{
		LOD 300

		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent-1"
			"IgnoreProjector" = "True"
			"PerformanceChecks" = "False"
		}

		// Depth Pass

		Pass
		{
			Name "DEPTH"

			ZWrite On
			ColorMask 0
		}

		// Forward Base Pass
		// Includes single directional light, emission and indirect lighting

		Pass
		{
			Name "FORWARD"

			Tags
			{
				"LightMode" = "ForwardBase"
			}

			Blend SrcAlpha OneMinusSrcAlpha
			Cull[_CullMode]
			ZWrite[_ZWrite]
			ZTest[_ZTest]

			CGPROGRAM
				#pragma target 3.0

				// Shader Features

				#pragma shader_feature ECS_DIFFUSEMAP
				#pragma shader_feature ECS_SURFACEMAP
				#pragma shader_feature ECS_NORMALMAP
				#pragma shader_feature _ ECS_DETAILMAP ECS_DETAILMAP2
				#pragma shader_feature ECS_DETAILNORMALMAP
				#pragma shader_feature ECS_DETAIL_UV2
				#pragma shader_feature ECS_DETAIL_MASKMAP
				#pragma shader_feature ECS_EMISSIONMAP 
				#pragma shader_feature ECS_DIRECTLIGHTING
				#pragma shader_feature ECS_OCCLUSIONMAP
				#pragma shader_feature _ ECS_LIGHTMAP ECS_DIRLIGHTMAP
				#pragma shader_feature _ ECS_INDIRECTSPECULAR_PROBE ECS_INDIRECTSPECULAR_CUSTOMCUBE ECS_INDIRECTSPECULAR_PLANAR
				#pragma shader_feature _ ECS_VERTEX_RGB_DIFFUSE ECS_VERTEX_RGB_EMISSIONCOLOR ECS_VERTEX_RGB_INDIRECTDIFFUSE
				#pragma shader_feature _ ECS_VERTEX_A_EMISSIONMASK ECS_VERTEX_A_DETAILMASK ECS_VERTEX_A_ALPHA

				// Multi Compiles

				#pragma multi_compile_fwdbase
				#pragma multi_compile_fog

				#if UNITY_VERSION >= 550
					#pragma multi_compile_instancing
				#endif

				// Vertex/Fragment Program

				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram

				// Defines

				#define ECS_ALPHAFADE
				#define ECS_PASS_FORWARDBASE

				// Includes

				#include "../PBR/CGIncludes/Config/Config.cginc"
			ENDCG
		}

		// Forward Additive Pass
		// Additively add one light per pass until maximum pixel light count is reached
			
		Pass 
		{
			Name "FORWARD_DELTA"

			Tags
			{
				"LightMode" = "ForwardAdd"
			}
					
			Blend One One
			Cull[_CullMode]
			ZWrite Off
			ZTest[_ZTest]

			Fog
			{ 
				Color ( 0, 0, 0, 0 )
			}

			CGPROGRAM
				#pragma target 3.0

				// Shader Features

				#pragma shader_feature ECS_DIFFUSEMAP
				#pragma shader_feature ECS_SURFACEMAP
				#pragma shader_feature ECS_NORMALMAP
				#pragma shader_feature _ ECS_DETAILMAP ECS_DETAILMAP2
				#pragma shader_feature ECS_DETAILNORMALMAP
				#pragma shader_feature ECS_DETAIL_UV2
				#pragma shader_feature ECS_DETAIL_MASKMAP
				#pragma shader_feature ECS_DIRECTLIGHTING
				#pragma shader_feature ECS_OCCLUSIONMAP
				#pragma shader_feature ECS_VERTEX_RGB_DIFFUSE
				#pragma shader_feature ECS_VERTEX_A_DETAILMASK ECS_VERTEX_A_ALPHA

				// Multi Compiles

				#pragma multi_compile_fwdadd_fullshadows
				#pragma multi_compile_fog

				// Skip Variants

				#pragma skip_variants LIGHTMAP_ON DYNAMICLIGHTMAP_ON DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE

				// Vertex/Fragment Program

				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram

				// Defines

				#define ECS_ALPHAFADE
				#define ECS_PASS_FORWARDADD

				// Includes

				#include "../PBR/CGIncludes/Config/Config.cginc"
			ENDCG
		}

		// Shadow Casting Pass

		Pass
		{
			Name "SHADOWCASTER"

			Tags
			{
				"LightMode" = "ShadowCaster"
			}

			ZWrite[_ZWrite]
			ZTest LEqual

			CGPROGRAM
				#pragma target 3.0

				// Shader Features

				#pragma shader_feature ECS_DIFFUSEMAP
				#pragma shader_feature ECS_VERTEX_A_ALPHA

				// Multi Compiles

				#pragma multi_compile_shadowcaster

				#if UNITY_VERSION >= 550
					#pragma multi_compile_instancing
				#endif

				// Vertex/Fragment Program

				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram

				// Defines

				#define ECS_ALPHABLEND
				#define ECS_PASS_SHADOWCASTING

				// Includes

				#include "../PBR/CGIncludes/Config/Config.cginc"
			ENDCG
		}
	}

	// Shader Model 2.5

	SubShader
	{
		LOD 250

		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent-1"
			"IgnoreProjector" = "True"
			"PerformanceChecks" = "False"
		}

		// Depth Pass

		Pass
		{
			Name "DEPTH"

			ZWrite On
			ColorMask 0
		}

		// Forward Base Pass
		// Includes single directional light, emission and indirect lighting

		Pass
		{
			Name "FORWARD"

			Tags
			{
				"LightMode" = "ForwardBase"
			}

			Blend SrcAlpha OneMinusSrcAlpha
			Cull[_CullMode]
			ZWrite[_ZWrite]
			ZTest[_ZTest]

			CGPROGRAM
				#pragma target 2.5

				// Shader Features

				#pragma shader_feature ECS_DIFFUSEMAP
				#pragma shader_feature ECS_SURFACEMAP
				#pragma shader_feature ECS_NORMALMAP
				#pragma shader_feature ECS_DETAILMAP
				#pragma shader_feature ECS_DETAIL_UV2
				#pragma shader_feature ECS_DETAIL_MASKMAP
				#pragma shader_feature ECS_EMISSIONMAP
				#pragma shader_feature ECS_DIRECTLIGHTING
				#pragma shader_feature ECS_OCCLUSIONMAP
				#pragma shader_feature _ ECS_LIGHTMAP ECS_DIRLIGHTMAP
				#pragma shader_feature _ ECS_INDIRECTSPECULAR_PROBE ECS_INDIRECTSPECULAR_CUSTOMCUBE
				#pragma shader_feature _ ECS_VERTEX_RGB_DIFFUSE ECS_VERTEX_RGB_EMISSIONCOLOR ECS_VERTEX_RGB_INDIRECTDIFFUSE
				#pragma shader_feature _ ECS_VERTEX_A_EMISSIONMASK ECS_VERTEX_A_DETAILMASK ECS_VERTEX_A_ALPHA

				// Multi Compiles

				#pragma multi_compile_fwdbase
				#pragma multi_compile_fog

				#if UNITY_VERSION >= 550
					#pragma multi_compile_instancing
				#endif

				// Skip Variants

				#pragma skip_variants SHADOWS_SOFT DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE

				#if defined(ECS_VERTEX_RGB_INDIRECTDIFFUSE)
					#pragma skip_variants DYNAMICLIGHTMAP_ON
				#endif

				#if defined(ECS_LIGHTMAP) || defined(ECS_DIRLIGHTMAP)
					#pragma skip_variants LIGHTMAP_ON
				#endif

				// Vertex/Fragment Program

				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram

				// Defines

				#define ECS_ALPHAFADE
				#define ECS_PASS_FORWARDBASE

				// Includes

				#include "../PBR/CGIncludes/Config/Config.cginc"
			ENDCG
		}

		// Forward Add Pass
		// Additively add one light per pass until maximum pixel light count is reached
			
		Pass 
		{
			Name "FORWARD_DELTA"

			Tags
			{
				"LightMode" = "ForwardAdd"
			}
					
			Blend One One
			Cull[_CullMode]
			ZWrite Off
			ZTest[_ZTest]

			Fog
			{ 
				Color ( 0, 0, 0, 0 )
			}
		
			CGPROGRAM
				#pragma target 2.5

				// Shader Features

				#pragma shader_feature ECS_DIFFUSEMAP
				#pragma shader_feature ECS_SURFACEMAP
				#pragma shader_feature ECS_NORMALMAP
				#pragma shader_feature ECS_DETAILMAP
				#pragma shader_feature ECS_DETAIL_UV2
				#pragma shader_feature ECS_DETAIL_MASKMAP
				#pragma shader_feature ECS_DIRECTLIGHTING
				#pragma shader_feature ECS_OCCLUSIONMAP
				#pragma shader_feature ECS_VERTEX_RGB_DIFFUSE
				#pragma shader_feature ECS_VERTEX_A_DETAILMASK ECS_VERTEX_A_ALPHA

				// Multi Compiles

				#pragma multi_compile_fwdadd_fullshadows
				#pragma multi_compile_fog

				// Skip Variants

				#pragma skip_variants SHADOWS_SOFT LIGHTMAP_ON DYNAMICLIGHTMAP_ON DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE

				// Vertex/Fragment Program

				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram

				// Defines

				#define ECS_ALPHAFADE
				#define ECS_PASS_FORWARDADD

				// Includes

				#include "../PBR/CGIncludes/Config/Config.cginc"
			ENDCG
		}

		// Shadow Casting Pass

		Pass
		{
			Name "SHADOWCASTER"

			Tags
			{
				"LightMode" = "ShadowCaster"
			}

			ZWrite[_ZWrite]
			ZTest LEqual

			CGPROGRAM
				#pragma target 2.5

				// Shader Features

				#pragma shader_feature ECS_DIFFUSEMAP
				#pragma shader_feature ECS_VERTEX_A_ALPHA

				// Multi Compiles

				#pragma multi_compile_shadowcaster

				#if UNITY_VERSION >= 550
					#pragma multi_compile_instancing
				#endif

				// Skip Variants

				#pragma skip_variants SHADOWS_SOFT

				// Vertex/Fragment Program

				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram

				// Difines

				#define ECS_ALPHABLEND
				#define ECS_PASS_SHADOWCASTING

				// Includes

				#include "../PBR/CGIncludes/Config/Config.cginc"
			ENDCG
		}
	}

	CustomEditor "ECS.Rendering.Shaders.PBRShaderInspector"
}