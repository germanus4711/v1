#region Header

// ------------------------------------------------------------------------------------------
// 
//   File Name: Method.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 23
//   Last Update: 2021 / 11 / 23
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------

#endregion

using System.Collections.Concurrent;
using System.Threading;
using UnityEngine;

namespace Threading
{
    public static class Method
    {
        public static void Do(object bag)
        {
            ConcurrentBag<int> concurrentBag = new ConcurrentBag<int>();
            concurrentBag = (ConcurrentBag<int>) bag; 
            var i = 0;
            for (; i < 5; i++)
            {
                Thread.Sleep(500);
                Debug.Log("xxxxx");
            }

            concurrentBag.Add(i * (concurrentBag.Count + 1));
        }

        public static void Do2(object o)
        {
            Do(o);
        }
    }
}