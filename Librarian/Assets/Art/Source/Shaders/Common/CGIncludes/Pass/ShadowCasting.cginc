//------------------------------------------------------------------------------------------
//
//  File Name:         	ShadowCasting.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	February 6, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_PASS_SHADOWCASTING_CGINC )
	#define ECS_PASS_SHADOWCASTING_CGINC
	
	// Unity Includes
	#include "UnityCG.cginc"
	
	// ECS Includes
	#include "../Common/CGIncludes/Utility/Utility.cginc"
	
	//--DEFINES-----------------------------------------------------------------------------
		
		// Do dithering for alpha blended shadows on SM3+/desktop;
		// on lesser systems do simple alpha-tested shadows
		#if defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE )
			#if !( ( SHADER_TARGET < 30 ) || defined( SHADER_API_MOBILE ) || defined( SHADER_API_D3D11_9X ) || defined( SHADER_API_PSP2 ) || defined( SHADER_API_PSM ) )
				#define ECS_USE_DITHER_MASK
			#endif
		#endif

		// Need to output UVs in shadow caster, since we need to sample texture and do clip/dithering based on it
		#if defined( ECS_ALPHATEST ) || defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE )
			#define ECS_USE_SHADOW_UVS
		#endif

		// Has a non-empty shadow caster output struct ( it's an error to have empty structs on some platforms... )
		#if !defined( V2F_SHADOW_CASTER_NOPOS_IS_EMPTY ) || defined( ECS_USE_SHADOW_UVS ) || defined( ECS_VERTEX_A_ALPHA ) || ( defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 ) )
			#define ECS_USE_SHADOW_OUTPUT_STRUCT
		#endif
		
	//--PASS SPECIFIC INPUTS----------------------------------------------------------------
	
		#if defined( ECS_USE_DITHER_MASK )
			sampler3D _DitherMaskLOD;
		#endif
	
	//--VERTEX PROGRAM----------------------------------------------------------------------
		
		// Vertex Program Input		
		struct VertexInput
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			#if defined( ECS_USE_SHADOW_UVS )
				float2 uv1 : TEXCOORD0;
			#endif
			
			#if defined( ECS_VERTEX_A_ALPHA ) || ( defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 ) )
			   	fixed4 color : COLOR;
			#endif

			#if UNITY_VERSION >= 550
				UNITY_VERTEX_INPUT_INSTANCE_ID
			#endif
		};
		
		// Vertex Program Output
		#if defined( ECS_USE_SHADOW_OUTPUT_STRUCT )
			struct VertexOutput
			{
				V2F_SHADOW_CASTER_NOPOS
				#if defined( ECS_USE_SHADOW_UVS )
					float2 uv1 : TEXCOORD1;
				#endif
				
				#if defined( ECS_VERTEX_A_ALPHA ) || ( defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 ) )
			   		fixed color : COLOR;
				#endif

				#if UNITY_VERSION >= 550
					UNITY_VERTEX_OUTPUT_STEREO
					UNITY_VERTEX_INPUT_INSTANCE_ID
				#endif
			};
		#endif

		// We have to do these dances of outputting SV_POSITION separately from the vertex shader,
		// and inputting VPOS in the pixel shader, since they both map to "POSITION" semantic on
		// some platforms, and then things don't go well.
		void VertexProgram( VertexInput v,
			#if defined( ECS_USE_SHADOW_OUTPUT_STRUCT )
				out VertexOutput vertexOutput,
			#endif
			out float4 opos : SV_POSITION )
		{
#if UNITY_VERSION >= 550
			UNITY_SETUP_INSTANCE_ID(v);
#endif	

			#if defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 )
				v.vertex.xyz = ECS_VertexWind( v.vertex.xyz, _WindStrength, _DirectionalWindStrength, v.color.a );
			#endif

			TRANSFER_SHADOW_CASTER_NOPOS( vertexOutput,opos )

			#if defined( ECS_USE_SHADOW_UVS )
				vertexOutput.uv1 = v.uv1.xy;
			#endif
			
			#if defined( ECS_VERTEX_A_ALPHA ) || ( defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 ) )
		   		vertexOutput.color = v.color.a;
			#endif
		}

	//--FRAGMENT PROGRAM--------------------------------------------------------------------

		half4 FragmentProgram( 
			#if defined( ECS_USE_SHADOW_OUTPUT_STRUCT )
				VertexOutput vertexOutput
			#endif
			#if defined( ECS_USE_DITHER_MASK )
				, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			 ) : SV_Target
		{
			#if defined( ECS_USE_SHADOW_UVS )	
				#if defined( ECS_INPUT_HAIR )
					half transparency = UNITY_SAMPLE_TEX2D( _HairMap, vertexOutput.uv1 ).b;

					#if defined( ECS_ALPHATEST )
							// Alpha Clip	
							clip( transparency - _ShadowCutoff );
					#elif defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE )
						transparency *= _ShadowCutoff;
					#endif
				#else
					#if defined( ECS_DIFFUSEMAP )
						half transparency = UNITY_SAMPLE_TEX2D( _MainTex, vertexOutput.uv1 ).a;
	
						#if defined( ECS_ALPHATEST )
								// Alpha Clip	
								clip( transparency - _Cutoff );
						#elif defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE )
							transparency *= _Cutoff;
						#endif
					#else
						half transparency = _Cutoff;
					#endif
				#endif
				
				#if defined( ECS_VERTEX_A_ALPHA )
					transparency *= vertexOutput.color;
				#endif
			
				#if defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE )
					#if defined( ECS_USE_DITHER_MASK )
						// Use dither mask for alpha blended shadows, based on pixel position xy
						// and alpha level. Our dither texture is 4x4x16.
						half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25h, transparency * 0.9375h ) ).a;
						clip( alphaRef - 0.01 );
					#endif
				#endif
			#endif
			SHADOW_CASTER_FRAGMENT( vertexOutput )
		}			
#endif