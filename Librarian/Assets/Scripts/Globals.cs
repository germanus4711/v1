using UnityEngine;

public static class Globals
{
    public static readonly string Localdb = $"URI=file:{Application.streamingAssetsPath}/Local.db";
    public static readonly Environment.Environments EnvironmentName = Environment.Environments.Development;
}