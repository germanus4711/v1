#region Header
// ------------------------------------------------------------------------------------------
// 
//   File Name: Test.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 17
//   Last Update: 2021 / 11 / 17
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------
#endregion

using System;
using System.Text;
using Mustache;
using UnityEngine;

namespace Templates
{
    public class Test : MonoBehaviour
    {
        public void Start()
        {
            FormatCompiler compiler = new FormatCompiler();
            
            System.IO.StreamReader file =  new System.IO.StreamReader("Assets/Scripts/Templates/Class.mus");

            StringBuilder sb = new StringBuilder();
            string line = string.Empty;
            
            while((line = file.ReadLine()) != null)
            {
                sb.Append(line);
            }

            file.Close();
            
            Generator generator = compiler.Compile(sb.ToString());
            // ----------
            
            string result = generator.Render(new
            {
                classname = "Awesome",
                field_type = "int",
                field_name = "TeaCup"
            });
            Debug.Log(result);
        }
    }
}