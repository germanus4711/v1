#region Header

// ------------------------------------------------------------------------------------------
// 
//   File Name: Spinoff.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 23
//   Last Update: 2021 / 11 / 23
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------

#endregion

using System.Collections.Concurrent;
using System.Threading;
using UnityEngine;

namespace Threading
{
    public class Spinoff : MonoBehaviour
    {
        private Thread _t1, _t2;
        private ConcurrentBag<int> _bag;

        public void Start()
        {
            _bag = new ConcurrentBag<int>();

            Debug.Log("> def t1");
            _t1 = new Thread(Method.Do);
            Debug.Log("> def t2");
            _t2 = new Thread(Method.Do2);
            if (_t1 == _t2) Debug.Log(">> t1 = t2");
            Debug.Log("> Calling!");
            Do();
            Debug.Log("> complete");
            int b = -1;
            while (!_bag.TryTake(out b))
            {
            }
            Debug.Log(b);
        }

        private void Do()
        {
            Debug.Log("> start t1");
            _t1.Start(_bag);
            Debug.Log("> start t2");
            _t2.Start(_bag);
            Debug.Log("> done");
        }
    }
}