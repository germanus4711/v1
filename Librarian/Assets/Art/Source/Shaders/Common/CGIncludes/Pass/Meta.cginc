//------------------------------------------------------------------------------------------
//
//  File Name:         	Meta.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#ifndef ECS_PASS_META_CGINC
	#define ECS_PASS_META_CGINC

	// Unity Includes
	#include "UnityCG.cginc"
	#include "UnityMetaPass.cginc"
	
	// ECS Includes
	#include "../Common/CGIncludes/Utility/Utility.cginc"
	
	//--PASS SPECIFIC INPUTS----------------------------------------------------------------
	
		#if !defined( ECS_VERTEXINPUT_UV2 )
			#define ECS_VERTEXINPUT_UV2
		#endif
		
		#if !defined( ECS_VERTEXINPUT_UV3 )
			#define ECS_VERTEXINPUT_UV3
		#endif

	//--VERTEX PROGRAM----------------------------------------------------------------------
	
		// Vertex Program Input
		#include "../Common/CGIncludes/Vertex/VertexInput.cginc"

		// Vertex Program Output
		#include "../Common/CGIncludes/Vertex/VertexOutput.cginc"

		VertexOutput VertexProgram ( VertexInput vertexInput )
		{
			VertexOutput vertexOutput;
			UNITY_INITIALIZE_OUTPUT( VertexOutput, vertexOutput );

			// Position
			vertexOutput.pos = UnityMetaVertexPosition( vertexInput.vertex, vertexInput.uv2.xy, vertexInput.uv3.xy, unity_LightmapST, unity_DynamicLightmapST );

			// UV1, UV2
				#if defined( ECS_VERTEXINPUT_UV1 ) && defined( ECS_INPUT_UV1_TRANSFORM )
					vertexInput.uv1.xy = vertexInput.uv1.xy * _UV1.x + _UV1.yz * _Time.y;
				#endif

				#if defined( VertexOutput_UV1 )
					vertexOutput.UV12.xy = vertexInput.uv1.xy;
					#if defined( VertexOutput_UV2 )
						#if defined( ECS_UNITY_LIGHTMAP )
							vertexOutput.UV12.zw = vertexInput.uv2.xy * unity_LightmapST.xy + unity_LightmapST.zw;
						#else
							vertexOutput.UV12.zw = vertexInput.uv2.xy;
						#endif
					#endif
				#elif defined( VertexOutput_UV2 )
					#if defined( ECS_UNITY_LIGHTMAP )
						vertexOutput.UV12.xy = vertexInput.uv2.xy * unity_LightmapST.xy + unity_LightmapST.zw;
					#else
						vertexOutput.UV12.xy = vertexInput.uv2.xy;
					#endif
				#endif
			return vertexOutput;
		}
#endif