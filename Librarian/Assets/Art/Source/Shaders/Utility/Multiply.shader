﻿//------------------------------------------------------------------------------------------
//
//  File Name:         	Multiply.shader
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	August 21, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

Shader "ECS/Utility/Multiply" 
{
	Properties 
	{
		_MainTex ( "Texture (G)", 2D ) = "white" {}
		_MultiplyStrength ( "Multiply Strength", Range( 0.001, 10.0 ) ) = 1.0
	}
		
	SubShader 
	{
		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent-1"
			"IgnoreProjector" = "True"
			"PerformanceChecks" = "False"
		}
		
		Pass
		{
			Blend DstColor Zero 
			Cull Back
			ZWrite Off
			ZTest LEqual
			Lighting Off 
			Fog
			{
				Color ( 0, 0, 0, 0 )
			}
	
			CGPROGRAM
				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram

				#include "UnityCG.cginc"

				uniform sampler2D _MainTex;
				uniform half _MultiplyStrength;
		
				struct VertexInput 
				{
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
				};

				struct VertexOutput 
				{
					float4 pos : SV_POSITION;
					float2 uv : TEXCOORD0;
				};
			
				VertexOutput VertexProgram( VertexInput v )
				{
					VertexOutput o;
					UNITY_INITIALIZE_OUTPUT( VertexOutput, o );

					#if ( UNITY_VERSION >= 550 )
						o.pos = UnityObjectToClipPos( v.vertex );
					#else
						o.pos = mul( UNITY_MATRIX_MVP, v.vertex );
					#endif

					o.uv = v.texcoord.xy;
					return o;
				}
			
				half4 FragmentProgram( VertexOutput i ) : SV_Target 
				{
					half result = pow( max( 0.001, tex2D( _MainTex, i.uv ).a ), _MultiplyStrength );
					return result;
				}
			ENDCG 
		}	
	}
}