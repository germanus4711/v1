//------------------------------------------------------------------------------------------
//
//  File Name:         	Fragment.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 14, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

#if !defined( ECS_FRAGMENT_CGINC )
	#define ECS_FRAGMENT_CGINC

	struct Fragment
	{
		// Diffuse
		half3 diffuse;

		// Transparency
		#if defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
			half transparency;
		#endif

		// Surface Properties
		half gloss;
		half roughness;
		half3 specular;

		// Normals
		half3 normalWorld;
		#if defined( ECS_DIRLIGHTMAP ) && ( ECS_SHADER_QUALITY == 1 ) && defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
			half3 normalTangent;
		#endif

		// Emission
		#if defined( ECS_INPUT_EMISSION ) || defined( ECS_DEBUG_EMISSION )
			half3 emission;
		#endif

		// Lightmaps
		#if defined( ECS_LIGHTMAP ) || defined( ECS_DIRLIGHTMAP )
			half3 lightmap;

			#if defined( ECS_DIRLIGHTMAP ) && ( ECS_SHADER_QUALITY == 1 ) && defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
			fixed3 lightmapDirectional;
			#endif
		#endif

		// Occlusion
		#if defined( ECS_INPUT_LIGHTING_OCCLUSION )
			fixed4 occlusion; // r = direct diffuse, g = direct specular, b = indirect diffuse, a = indirect specular
		#endif

		// Planar Reflection
		#if defined( ECS_INDIRECTSPECULAR_PLANAR ) && ( ECS_SHADER_QUALITY == 1 )
			half3 planarReflection;
		#endif

		// Vectors
		#if defined( ECS_INPUT_LIGHTING_DIRECT ) || defined( ECS_INPUT_LIGHTING_INDIRECT )
			half3 eyeDirection;
			half dotNV;
		#endif
	};
#endif