package com.ecs.lumber;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/record")
@Consumes(MediaType.TEXT_PLAIN)
public class Endpoints {
    @GET
    @Produces("text/plain")
    public String record(
            @DefaultValue("N/A") @QueryParam("log") String log
    ) {
        System.out.println(log);
        return "Debug.Log: " + log;
    }
}