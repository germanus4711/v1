using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq.Mapping;
using Mono.Data.Sqlite;

namespace dbo
{
    [Table(Name = "Highscore")]
    public class Highscore
    {
        private readonly string _dbname;

        [Column(Name = "name")] public string Name { get; set; }

        [Column(Name = "score")] public int Score { get; set; }

        public Highscore()
        {
            _dbname = Globals.Localdb;
        }

        public Highscore(string dbname)
        {
            _dbname = dbname;
        }

        private int ExecuteDB(string query, Dictionary<string, object> args)
        {
            //setup the connection to the database
            using var con = new SqliteConnection(_dbname);
            con.Open();

            //open a new command
            using var cmd = con.CreateCommand();
            cmd.CommandText = query;
            //set the arguments given in the query
            foreach (var pair in args)
            {
                cmd.Parameters.AddWithValue(pair.Key, pair.Value);
            }

            //execute the query and get the number of row affected
            var numberOfRowsAffected = cmd.ExecuteNonQuery();

            cmd.Dispose();
            con.Close();

            return numberOfRowsAffected;
        }

        private List<Highscore> ReadDB(string query, Dictionary<string, object> args)
        {
            if (string.IsNullOrEmpty(query.Trim()))
                return null;
            List<Highscore> highscores = null;
            try
            {
                using var con = new SqliteConnection(_dbname);

                con.Open();

                //open a new command
                using var cmd = con.CreateCommand();
                cmd.CommandText = query;
                foreach (KeyValuePair<string, object> entry in args)
                {
                    cmd.Parameters.AddWithValue(entry.Key, entry.Value);
                }

                highscores = new List<Highscore>();

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Highscore hs = new Highscore
                        {
                            Name = reader.GetString(0),
                            Score = reader.GetInt32(1)
                        };

                        highscores.Add(hs);
                    }

                    reader.Close();
                }
                cmd.Dispose();
                con.Close();
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log(e);
            }

            return highscores;
        }

        public int Insert()
        {
            const string query = "INSERT INTO Highscore(name, score) VALUES(@name, @score)";

            //here we are setting the parameter values that will be actually 
            //replaced in the query in Execute method
            var args = new Dictionary<string, object>
            {
                {"@name", Name},
                {"@score", Score}
            };

            return ExecuteDB(query, args);
        }


        public int Delete()
        {
            const string query = "DELETE FROM Highscore WHERE name =  @name";

            //here we are setting the parameter values that will be actually 
            //replaced in the query in Execute method
            var args = new Dictionary<string, object>
            {
                {"@name", Name}
            };

            return ExecuteDB(query, args);
        }

        public int Remove()
        {
            const string query = "UPDATE Highscore SET name = '-' + @name WHERE name = @name";

            //here we are setting the parameter values that will be actually 
            //replaced in the query in Execute method
            var args = new Dictionary<string, object>
            {
                {"@name", Name}
            };

            return ExecuteDB(query, args);
        }

        public int Update()
        {
            const string query = "UPDATE Highscore SET score = @score WHERE name = @name";

            //here we are setting the parameter values that will be actually 
            //replaced in the query in Execute method
            var args = new Dictionary<string, object>
            {
                {"@name", Name},
                {"@score", Score}
            };

            return ExecuteDB(query, args);
        }


        public List<Highscore> Read()
        {
            var query = "SELECT name, score FROM Highscore WHERE ";
            var args = new Dictionary<string, object>();
            //here we are setting the parameter values that will be actually 
            //replaced in the query in Execute method
            if (Name.Length > 0)
            {
                query += "name = @name";
                args.Add("@name", Name);
            }
            else
            {
                query += "score = @score";
                args.Add("@score", Score);
            }

            var hsl = ReadDB(query, args);

            return hsl;
        }
    }
}