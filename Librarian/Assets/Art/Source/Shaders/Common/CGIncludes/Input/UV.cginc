//------------------------------------------------------------------------------------------
//
//  File Name:         	UV.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_UV_TRANSFORM ) && ( !defined( ECS_PASS_SHADOWCASTING ) || ( defined( ECS_PASS_SHADOWCASTING ) && ( defined( ECS_ALPHATEST ) || defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) ) ) )
	#define ECS_INPUT_UV_TRANSFORM

	// UV1 Transform
	#if defined( ECS_VERTEXINPUT_UV1 ) && defined( ECS_INPUT_UV1_TRANSFORM )
		uniform float3 _UV1;
	#endif

	// UV2 Transform
	#if defined( ECS_VERTEXINPUT_UV2 ) && defined( ECS_INPUT_UV2_TRANSFORM )
		uniform float3 _UV2;
	#endif

	// UV3 Transform
	#if defined( ECS_VERTEXINPUT_UV3 ) && defined( ECS_INPUT_UV3_TRANSFORM )
		uniform float3 _UV3;
	#endif

	// UV4 Transform
	#if defined( ECS_VERTEXINPUT_UV4 ) && defined( ECS_INPUT_UV4_TRANSFORM )
			uniform float3 _UV4;
	#endif
#endif