#region Header
// ------------------------------------------------------------------------------------------
// 
//   File Name: RunMigration.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 19
//   Last Update: 2021 / 11 / 19
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------
#endregion

using Remote.DB.Database;
using UnityEditor;
using UnityEngine;

namespace Remote.DB.Editor
{
    public class MigrationEditor : EditorWindow
    {
        [MenuItem("Tools/Migrate DB")]
        public static void OpenWindow()
        {
            MigrationEditor window = (MigrationEditor) GetWindow(typeof(MigrationEditor));
            window.titleContent = new GUIContent("Migrate DB");
            window.Show();
        }

        void OnGUI()
        {
            EditorGUILayout.LabelField("Migrate Dragon DB", EditorStyles.boldLabel);
            EditorGUILayout.Separator();
            var result = V1_Migration.Database();
            UnityEngine.Debug.Log(">>> " + result);
            EditorGUILayout.LabelField(result);
            if (GUILayout.Button("Close!"))
                Close();
            // V1_Migration.Database(); // Migrate the db !
        }
    }
}