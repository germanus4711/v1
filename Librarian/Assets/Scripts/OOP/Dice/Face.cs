using System.Collections.Generic;
using UnityEngine;

namespace OOP.Dice
{
    public class Face
    {
        /* fact */
        [SerializeField]
        private System.Guid id;
        private List<Vector3> _points;
        private static int minPoints = 3;
        private Material _material;
    
        /* To be Thought of */
        // TODO define material w/ Artist!
        // private List<Material> _allowed;

        public Face()
        {
            /* setup, populating facts to be "real" */
            id = new System.Guid();
        }

        public List<Vector3> Points
        {
            get => _points;
            set => _points = value;
        }

        public Material Material
        {
            get => _material;
            set => _material = value;
        }

        public bool HasMinimal()
        {
            return minPoints == _points.Count;
        }

        public override string ToString()
        {
            // base.ToString();
            return id.ToString();
        }
    }
}