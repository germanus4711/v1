//------------------------------------------------------------------------------------------
//
//  File Name:         	IndirectLighting.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_LIGHTING_INDIRECT ) && ( defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_DEFERRED ) )
	#define ECS_INPUT_LIGHTING_INDIRECT

	//--INDIRECT DIFFUSE--------------------------------------------------------------------

		//--LIGHTMAPS

			// Texture : ECS Baked Non-Directional
			#if defined( ECS_LIGHTMAP )
				#if !defined( ECS_VERTEXINPUT_UV2 )
					#define ECS_VERTEXINPUT_UV2
				#endif
				
				#if !defined( ECS_VERTEXOUTPUT_UV2 )
					#define ECS_VERTEXOUTPUT_UV2
				#endif
				uniform UNITY_DECLARE_TEX2D( _LightingMap );
				uniform half4 _LightingMap_HDR;
		
			// Texture : ECS Baked Directional
			#elif defined( ECS_DIRLIGHTMAP )
				#if !defined( ECS_VERTEXINPUT_UV2 )
					#define ECS_VERTEXINPUT_UV2
				#endif
				
				#if !defined( ECS_VERTEXOUTPUT_UV2 )
					#define ECS_VERTEXOUTPUT_UV2
				#endif
				uniform UNITY_DECLARE_TEX2D( _LightingMap );
				uniform half4 _LightingMap_HDR;

				uniform UNITY_DECLARE_TEX2D_NOSAMPLER( _DirLightingMap );

				#if !defined( ECS_INPUT_LIGHTING_OCCLUSION )
					#define ECS_INPUT_LIGHTING_OCCLUSION
				#endif

			// Texture : Unity Baked
			#elif !defined( LIGHTMAP_OFF )
				#define ECS_UNITY_LIGHTMAP
				
				#if !defined( ECS_VERTEXINPUT_UV2 )
					#define ECS_VERTEXINPUT_UV2
				#endif

				#if !defined( ECS_VERTEXOUTPUT_UV2 )
					#define ECS_VERTEXOUTPUT_UV2
				#endif
			#endif

			// Texture : Unity Dynamic
			#if defined( DYNAMICLIGHTMAP_ON )
				#define ECS_UNITY_DYNAMICLIGHTMAP
				
				#if !defined( ECS_VERTEXINPUT_UV3 )
					#define ECS_VERTEXINPUT_UV3
				#endif

				#if !defined( ECS_VERTEXOUTPUT_UV3 )
					#define ECS_VERTEXOUTPUT_UV3
				#endif
			#endif	
	
		//--UNITY SPHERICAL HARMONICS
		// If we don't have a lightmap we sample Unity's spherical harmonics ( Ambient Source/Light Probes )

			#if !defined( ECS_LIGHTMAP ) && !defined( ECS_DIRLIGHTMAP ) && defined( LIGHTMAP_OFF ) && !defined( DYNAMICLIGHTMAP_ON )
				#define ECS_VERTEXOUTPUT_INDIRECTDIFFUSE
				#define ECS_UNITY_SH

				// Light Probe Proxy Volume
				#if ( ECS_SHADER_QUALITY == 1 ) && defined( UNITY_LIGHT_PROBE_PROXY_VOLUME )
					#if !defined( ECS_VERTEXOUTPUT_WORLDPOSITION )
						#define ECS_VERTEXOUTPUT_WORLDPOSITION
					#endif
				#endif
			
				// Vertex Color RGB
				#if defined( ECS_VERTEX_RGB_INDIRECTDIFFUSE )
					#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
						#define ECS_VERTEXINPUT_VERTEXCOLORS
					#endif

					#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS_GAMMA )
						#define ECS_VERTEXINPUT_VERTEXCOLORS_GAMMA
					#endif
				
					#if ( ECS_SHADER_QUALITY == 1 ) && defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD ) && !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
						#define ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB
					#endif
				// Color Picker
				#else
					uniform half3 _IndirectDiffuseColor;
				#endif
			#endif
			
	//--INDIRECT SPECULAR-------------------------------------------------------------------

		// Unity Reflection Probe Input
		#if defined( ECS_INDIRECTSPECULAR_PROBE ) && !( defined( ECS_PASS_DEFERRED ) && defined( UNITY_ENABLE_REFLECTION_BUFFERS ) )
			#if !defined( ECS_INDIRECTSPECULAR )
				#define ECS_INDIRECTSPECULAR
			#endif

			#if ( ECS_SHADER_QUALITY == 1 )
				#if !defined( ECS_VERTEXOUTPUT_WORLDPOSITION )
					#define ECS_VERTEXOUTPUT_WORLDPOSITION
				#endif
			#endif
		#endif
	
		// Custom Cubemap Input
		#if defined( ECS_INDIRECTSPECULAR_CUSTOMCUBE )
			#if !defined( ECS_INDIRECTSPECULAR )
				#define ECS_INDIRECTSPECULAR
			#endif

			UNITY_DECLARE_TEXCUBE( _IndirectSpecularCube );
			uniform half4 _IndirectSpecularCube_HDR;
		#endif
		
		// Planar Reflections Input
		#if defined( ECS_INDIRECTSPECULAR_PLANAR )
			#if !defined( ECS_INDIRECTSPECULAR )
				#define ECS_INDIRECTSPECULAR
			#endif

			#if !defined( ECS_VERTEXOUTPUT_SCREENPOSITION )
				#define ECS_VERTEXOUTPUT_SCREENPOSITION
			#endif

			uniform sampler2D _PlanarReflection;
		#endif
#endif