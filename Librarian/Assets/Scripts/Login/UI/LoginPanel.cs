using System;
using UI;
using UnityEngine;

namespace Login.UI
{
    public class LoginPanel : MonoBehaviour
    {
        enum Type
        {
            Email,
            UserName,
            CAPTCHA,
            Count
        }

        private void Awake()
        {
        }

        private void Start()
        {
            var panel = Make.HUD();
            Login(panel);
            Captcha(panel);
            Password(panel);
        }


        private GameObject Login(GameObject vGroup)
        {
            var hzGroup = Make.HorizontalGroup(vGroup, "HZ_Login");

            var login = MakeLogin(Type.Email, hzGroup);

            return hzGroup;
        }
        
        private GameObject Captcha(GameObject vGroup)
        {
            var hzGroup = Make.HorizontalGroup(vGroup, "HZ_Login");

            var login = MakeLogin(Type.CAPTCHA, hzGroup);

            return hzGroup;
        }

        private GameObject Password(GameObject vGroup)
        {
            var hzGroup = Make.HorizontalGroup(vGroup, "HZ_Passwd");

            var password = Make.Password(hzGroup, "Enter your Password:");

            var passwd = Make.GetPassword(hzGroup, ProcessPassword);
            Make.UIObjectInit(passwd, hzGroup);

            return hzGroup;
        }

        private void ProcessPassword()
        {
            throw new NotImplementedException();
        }


        private void Update()
        {
        }

        private GameObject MakeLogin(Type type, GameObject hzGroup)
        {
            switch (type)
            {
                case Type.Email:
                    return DoEmail(hzGroup);
                    // break;
                case Type.UserName:
                    return DoUserName(hzGroup);
                    // break;
                case Type.CAPTCHA:
                    return DoCaptcha(hzGroup);
                    // break;
                default:
                    throw new ArgumentException("Invalid Login Type!");
                    // break;
            }
        }

        private GameObject DoCaptcha(GameObject hzGroup)
        {
            throw new NotImplementedException();
        }

        private GameObject DoUserName(GameObject hzGroup)
        {
            throw new NotImplementedException();
        }

        private GameObject DoEmail(GameObject hzGroup)
        {
            var go = new GameObject
            {
                name = "Email",
                layer = 5
            };

            Make.ExpandRect(go, hzGroup);

            Make.Text(go, "Enter your Email:");
            Make.UIObjectInit(go, hzGroup);
            
            return go;
        }


    }
}