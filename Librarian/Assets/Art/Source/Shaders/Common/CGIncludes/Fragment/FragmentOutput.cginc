//------------------------------------------------------------------------------------------
//
//  File Name:         	FragmentOutput.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	June 4, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_FRAGMENTOUTPUT_CGINC )
	#define ECS_FRAGMENTOUTPUT_CGINC

	inline void ECS_Fragment_Setup ( inout Fragment fragment, VertexOutput vertexOutput )
	{
		//--LAYER BLENDING------------------------------------------------------------------
		
			#if defined( ECS_INPUT_BLEND )
				// Texture Blend Mask
				#if defined( ECS_BLEND_TEXTURE )
					#if defined( ECS_VERTEXOUTPUT_UV3 )
						#if defined( ECS_BLEND_3 )
							fixed2 blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.zw ).rg;
						#elif defined( ECS_BLEND_4 )
							fixed3 blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.zw ).rgb;
						#elif defined( ECS_BLEND_5 )
							fixed4 blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.zw ).rgba;
						#else
							fixed blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.zw ).r;
						#endif
					#else
						#if defined( ECS_BLEND_3 )
							fixed2 blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.xy ).rg;
						#elif defined( ECS_BLEND_4 )
							fixed3 blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.xy ).rgb;
						#elif defined( ECS_BLEND_5 )
							fixed4 blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.xy ).rgba;
						#else
							fixed blendMask = UNITY_SAMPLE_TEX2D( _BlendMap, vertexOutput.UV34.xy ).r;
						#endif
					#endif
				// Vertex Colors Blend Mask
				#else
					#if defined( ECS_BLEND_3 )
						fixed2 blendMask = vertexOutput.color.rg;
					#elif defined( ECS_BLEND_4 )
						fixed3 blendMask = vertexOutput.color.rgb;
					#elif defined( ECS_BLEND_5 )
						fixed4 blendMask = vertexOutput.color.rgba;
					#else
						fixed blendMask = vertexOutput.color.r;
					#endif
				#endif

				// Threshold
				#if ( ECS_SHADER_QUALITY == 1 )
					#if defined( ECS_BLEND_3 )
						blendMask = smoothstep( _BlendMinThreshold.xy, _BlendMaxThreshold.xy, blendMask );	
					#elif defined( ECS_BLEND_4 )
						blendMask = smoothstep( _BlendMinThreshold.xyz, _BlendMaxThreshold.xyz, blendMask );	
					#elif defined( ECS_BLEND_5 )
						blendMask = smoothstep( _BlendMinThreshold.xyzw, _BlendMaxThreshold.xyzw, blendMask );	
					#else
						blendMask = smoothstep( _BlendMinThreshold.x, _BlendMaxThreshold.x, blendMask );	
					#endif
				#endif

				// Blend UV Coordinates
				#if defined( ECS_INPUT_BLEND ) && ( defined( ECS_DIFFUSEMAP ) || defined( ECS_NORMALMAP ) )		
					float2 blend2UV = vertexOutput.UV12.xy * _BlendTiling.x;
					#if defined( ECS_BLEND_3 )
						float2 blend3UV = vertexOutput.UV12.xy * _BlendTiling.y;
					#elif defined( ECS_BLEND_4 )
						float2 blend3UV = vertexOutput.UV12.xy * _BlendTiling.y;
						float2 blend4UV = vertexOutput.UV12.xy * _BlendTiling.z;
					#elif defined( ECS_BLEND_5 )
						float2 blend3UV = vertexOutput.UV12.xy * _BlendTiling.y;
						float2 blend4UV = vertexOutput.UV12.xy * _BlendTiling.z;
						float2 blend5UV = vertexOutput.UV12.xy * _BlendTiling.w;
					#endif
				#endif	
			#endif			

		//--DIFFUSE/TRANSPARENCY------------------------------------------------------------
	
			#if defined( ECS_INPUT_DIFFUSE )
					fragment.diffuse = 1.0h;

					#if defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
						fragment.transparency = 1.0h;
					#endif

				// Texture
				#if defined( ECS_DIFFUSEMAP )
					#if defined( ECS_ALPHATEST ) || defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
						half4 diffuse = UNITY_SAMPLE_TEX2D( _MainTex, vertexOutput.UV12.xy ).rgba;
						fragment.diffuse *= diffuse.rgb;		
						#if defined( ECS_ALPHATEST )
							clip( diffuse.a - _Cutoff );
						#elif defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
							#if defined( ECS_ALPHATEST_SOFTEN )
								clip( -( diffuse.a - _Cutoff ) );
							#endif
							fragment.transparency *= diffuse.a;
						#endif
					#else
						#if defined( ECS_SHADER_LEGACY ) && defined( ECS_GLOSSMAP )
							half4 diffuse = UNITY_SAMPLE_TEX2D( _MainTex, vertexOutput.UV12.xy ).rgba;
							#if defined( ECS_INPUT_BLEND )
								diffuse = lerp( diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex2, _MainTex, blend2UV ).rgba, blendMask.r );
								#if defined( ECS_BLEND_3 )
									diffuse = lerp( diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex3, _MainTex, blend3UV ).rgba, blendMask.g );
								#elif defined( ECS_BLEND_4 )
									diffuse = lerp( diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex3, _MainTex, blend3UV ).rgba, blendMask.g );
									diffuse = lerp( diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex4, _MainTex, blend4UV ).rgba, blendMask.b );
								#elif defined( ECS_BLEND_5 )
									diffuse = lerp( diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex3, _MainTex, blend3UV ).rgba, blendMask.g );
									diffuse = lerp( diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex4, _MainTex, blend4UV ).rgba, blendMask.b );
									diffuse = lerp( diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex5, _MainTex, blend5UV ).rgba, blendMask.a );
								#endif
							#endif
							fragment.diffuse *= diffuse.rgb;
						#else
							half3 diffuse = UNITY_SAMPLE_TEX2D( _MainTex, vertexOutput.UV12.xy ).rgb;
							#if defined( ECS_INPUT_BLEND )
								diffuse = lerp( fragment.diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex2, _MainTex, blend2UV ).rgb, blendMask.r );
								#if defined( ECS_BLEND_3 )
									diffuse = lerp( fragment.diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex3, _MainTex, blend3UV ).rgb, blendMask.g );
								#elif defined( ECS_BLEND_4 )
									diffuse = lerp( fragment.diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex3, _MainTex, blend3UV ).rgb, blendMask.g );
									diffuse = lerp( fragment.diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex4, _MainTex, blend4UV ).rgb, blendMask.b );
								#elif defined( ECS_BLEND_5 )
									diffuse = lerp( fragment.diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex3, _MainTex, blend3UV ).rgb, blendMask.g );
									diffuse = lerp( fragment.diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex4, _MainTex, blend4UV ).rgb, blendMask.b );
									diffuse = lerp( fragment.diffuse, UNITY_SAMPLE_TEX2D_SAMPLER( _MainTex5, _MainTex, blend5UV ).rgb, blendMask.a );
								#endif
							#endif
							fragment.diffuse *= diffuse;
						#endif
					#endif		
				#endif

				// Vertex Colors
				#if defined( ECS_VERTEX_RGB_DIFFUSE )
					fragment.diffuse *= vertexOutput.color.rgb;
				#endif

				// Color Picker
				half3 color = _Color;

				#if defined( ECS_INPUT_BLEND )						
					color = lerp( color, _Color2, blendMask.r );
					#if defined( ECS_BLEND_3 )
						color = lerp( color, _Color3, blendMask.g );
					#elif defined( ECS_BLEND_4 )
						color = lerp( color, _Color3, blendMask.g );
						color = lerp( color, _Color4, blendMask.b );
					#elif defined( ECS_BLEND_5 )
						color = lerp( color, _Color3, blendMask.g );
						color = lerp( color, _Color4, blendMask.b );
						color = lerp( color, _Color5, blendMask.a );
					#endif
				#endif

				#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_DEFERRED ) || defined( ECS_PASS_META )
					fragment.diffuse *= lerp(color, _HighlightColor.rgb, _HighlightColor.a);
				#else
					fragment.diffuse *= color;
				#endif

				// Transparency
				#if defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
					fragment.transparency *= _Cutoff;
					#if defined( ECS_VERTEX_A_ALPHA )
						#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
							fragment.transparency *= vertexOutput.color.a;
						#else
							fragment.transparency *= vertexOutput.color;
						#endif
					#endif
				#endif

				// Colorspace Correction
				#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
					if ( ECS_IsGammaSpace(  ) )
						fragment.diffuse = ECS_GammaToLinear( fragment.diffuse );
				#endif
			#endif

		//--LIGHTING---------------------------------------------------------------
			
			// Indirect Diffuse
			#if defined( ECS_INPUT_LIGHTING_INDIRECT )
				// Lightmaps
				#if defined( ECS_LIGHTMAP ) || defined( ECS_DIRLIGHTMAP )
					#if defined( ECS_VERTEXOUTPUT_UV1 )
						fixed4 lightmap = UNITY_SAMPLE_TEX2D( _LightingMap, vertexOutput.UV12.zw );
					#else
						fixed4 lightmap = UNITY_SAMPLE_TEX2D( _LightingMap, vertexOutput.UV12.xy );
					#endif
	
					fragment.lightmap = ECS_DecodeRGBM( lightmap, _LightingMap_HDR );
	
					// Colorspace Correction
					#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
						if ( ECS_IsGammaSpace(  ) )
							fragment.lightmap = ECS_GammaToLinear( fragment.lightmap );
					#endif
	
					#if defined( ECS_DIRLIGHTMAP ) && ( ECS_SHADER_QUALITY == 1 ) && defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
						#if defined( ECS_VERTEXOUTPUT_UV1 )
							fixed3 lightmapDirectional = UNITY_SAMPLE_TEX2D_SAMPLER( _DirLightingMap, _LightingMap, vertexOutput.UV12.zw );
						#else
							fixed3 lightmapDirectional = UNITY_SAMPLE_TEX2D_SAMPLER( _DirLightingMap, _LightingMap, vertexOutput.UV12.xy );
						#endif
						
						fragment.lightmapDirectional = fixed3( lightmapDirectional.r, lightmapDirectional.g, 0.75 );
					#endif
				#endif
			#endif

			// Occlusion
			#if defined( ECS_INPUT_LIGHTING_OCCLUSION ) && ( defined( ECS_INPUT_LIGHTING_DIRECT ) || defined( ECS_INPUT_LIGHTING_INDIRECT ) )
				fragment.occlusion = 1.0h;

				// Texture
				#if defined( ECS_DIRLIGHTMAP )
					#if ( ECS_SHADER_QUALITY == 1 ) && defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
						fragment.occlusion = lightmapDirectional.b;
					#else
						#if defined( ECS_VERTEXOUTPUT_UV1 )
							fragment.occlusion = UNITY_SAMPLE_TEX2D_SAMPLER( _DirLightingMap, _LightingMap, vertexOutput.UV12.zw ).b;
						#else
							fragment.occlusion = UNITY_SAMPLE_TEX2D_SAMPLER( _DirLightingMap, _LightingMap, vertexOutput.UV12.xy ).b;
						#endif
					#endif
				#elif defined( ECS_OCCLUSIONMAP )
					#if defined( ECS_VERTEXOUTPUT_UV1 )
						fragment.occlusion = UNITY_SAMPLE_TEX2D( _OcclusionMap, vertexOutput.UV12.zw ).a;
					#else
						fragment.occlusion = UNITY_SAMPLE_TEX2D( _OcclusionMap, vertexOutput.UV12.xy ).a;
					#endif
				#endif
				
				// Vertex Color Alpha
				#if defined( ECS_VERTEX_A_OCCLUSION )
					#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
						fragment.occlusion *= vertexOutput.color.a;
					#else
						fragment.occlusion *= vertexOutput.color;
					#endif
				#endif

				#if defined( ECS_LIGHTMAP )
					fragment.occlusion.b = 1.0h;
				#endif
			#endif

		// Normals ( Tangent Space )
		
		#if defined( ECS_INPUT_NORMAL )

			#if defined( ECS_NORMALMAP )
				half4 normalPacked = UNITY_SAMPLE_TEX2D( _BumpMap, vertexOutput.UV12.xy ).rgba;

				#if defined( ECS_INPUT_BLEND )
					normalPacked = lerp( normalPacked, UNITY_SAMPLE_TEX2D_SAMPLER( _BumpMap2, _BumpMap, blend2UV ).rgba, blendMask.r );

					#if defined( ECS_BLEND_3 )
						normalPacked = lerp( normalPacked, UNITY_SAMPLE_TEX2D_SAMPLER( _BumpMap3, _BumpMap, blend3UV ).rgba, blendMask.g );
					#elif defined( ECS_BLEND_4 )
						normalPacked = lerp( normalPacked, UNITY_SAMPLE_TEX2D_SAMPLER( _BumpMap3, _BumpMap, blend3UV ).rgba, blendMask.g );
						normalPacked = lerp( normalPacked, UNITY_SAMPLE_TEX2D_SAMPLER( _BumpMap4, _BumpMap, blend4UV ).rgba, blendMask.b );
					#elif defined( ECS_BLEND_5 )
						normalPacked = lerp( normalPacked, UNITY_SAMPLE_TEX2D_SAMPLER( _BumpMap3, _BumpMap, blend3UV ).rgba, blendMask.g );
						normalPacked = lerp( normalPacked, UNITY_SAMPLE_TEX2D_SAMPLER( _BumpMap4, _BumpMap, blend4UV ).rgba, blendMask.b );
						normalPacked = lerp( normalPacked, UNITY_SAMPLE_TEX2D_SAMPLER( _BumpMap5, _BumpMap, blend5UV ).rgba, blendMask.a );
					#endif

				#endif

				#if ( ECS_SHADER_QUALITY == 1 )

					#if defined( ECS_DIRLIGHTMAP )
						fragment.normalTangent = ECS_UnpackScaleNormal( normalPacked, _BumpStrength );
					#else
						half3 normalTangent = ECS_UnpackScaleNormal( normalPacked, _BumpStrength );
					#endif

				#else
					half3 normalTangent = ECS_UnpackNormal( normalPacked );
				#endif

			#endif

		#endif

		//--DETAIL--------------------------------------------------------------------------
		
			#if defined( ECS_INPUT_DETAIL )
				#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
					half2 detailStrength = 1.0;
				#else
					half detailStrength = 1.0;
				#endif
			
				// UV Coordinates
				#if defined( ECS_DETAILMAP ) || ( defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 ) ) || ( defined( ECS_DETAILNORMALMAP ) && ( ECS_SHADER_QUALITY == 1 ) )
					#if defined( ECS_DETAIL_UV2 )	
						#if defined( ECS_VERTEXOUTPUT_UV1 )
							#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
								float4 detailUV = float4( vertexOutput.UV12.z * _DetailTiling, vertexOutput.UV12.w * _DetailTiling, vertexOutput.UV12.z * _Detail2Tiling, vertexOutput.UV12.w * _Detail2Tiling );
							#else
								float2 detailUV = vertexOutput.UV12.zw * _DetailTiling;
							#endif
						#else
							#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
								float4 detailUV = float4( vertexOutput.UV12.x * _DetailTiling, vertexOutput.UV12.y * _DetailTiling, vertexOutput.UV12.x * _Detail2Tiling, vertexOutput.UV12.y * _Detail2Tiling );
							#else
								float2 detailUV = vertexOutput.UV12.xy * _DetailTiling;
							#endif
						#endif
					#else
						#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
							float4 detailUV = float4( vertexOutput.UV12.x * _DetailTiling, vertexOutput.UV12.y * _DetailTiling, vertexOutput.UV12.x * _Detail2Tiling, vertexOutput.UV12.y * _Detail2Tiling );
						#else
							float2 detailUV = vertexOutput.UV12.xy * _DetailTiling;
						#endif
					#endif
				#endif
				
				// Mask
				#if defined( ECS_DETAIL_MASKMAP ) || defined( ECS_VERTEX_A_DETAILMASK )
					half detailMask = 1.0h;

					#if defined( ECS_DETAIL_MASKMAP )
						detailMask = UNITY_SAMPLE_TEX2D( _DetailMaskMap, vertexOutput.UV12.xy ).r;
					#endif

					#if defined( ECS_VERTEX_A_DETAILMASK )
						#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
							detailMask *= vertexOutput.color.a;
						#else
							detailMask *= vertexOutput.color;
						#endif
					#endif

					detailStrength *= detailMask;
				#endif
				
				// Texture
				#if defined( ECS_DETAILMAP ) || ( defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 ) )
					#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
						detailStrength *= half2( _DetailStrength, _Detail2Strength );
					#else
						detailStrength *= _DetailStrength; 
					#endif

					// PBR Shader
					#if defined( ECS_SHADER_PBR )
						fixed4 detailMap = UNITY_SAMPLE_TEX2D( _DetailMap, detailUV.xy ).rgba;
						fragment.occlusion *= detailMap.a;
						half3 detail = detailMap.rgb * 2.0h;

						#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
							fixed4 detailMap2 = UNITY_SAMPLE_TEX2D( _Detail2Map, detailUV.zw ).rgba;
							fragment.occlusion *= detailMap2.a;
							half3 detail2 = detailMap2.rgb * 2.0h;

							fragment.diffuse = max( half3( 0, 0, 0 ), fragment.diffuse * ECS_LerpOneTo( detail.r, detailStrength.x ) * ECS_LerpOneTo( detail2.r, detailStrength.y ) );
						#else
							fragment.diffuse = max( half3( 0, 0, 0 ), fragment.diffuse * ECS_LerpOneTo( detail.r, detailStrength.x ) );
						#endif

					// Legacy Shader
					#elif defined( ECS_SHADER_LEGACY )
						half2 detail = UNITY_SAMPLE_TEX2D( _DetailMap, detailUV.xy ).rg * 2.0h;

						#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
							half2 detail2 = UNITY_SAMPLE_TEX2D( _Detail2Map, detailUV.zw ).rg * 2.0h;
							fragment.diffuse = max( half3( 0, 0, 0 ), fragment.diffuse * ECS_LerpOneTo( detail.r, detailStrength.x ) * ECS_LerpOneTo( detail2.r, detailStrength.y ) );
						#else
							fragment.diffuse = max( half3( 0, 0, 0 ), fragment.diffuse * ECS_LerpOneTo( detail.r, detailStrength.x ) );
						#endif
					#endif
				#endif
				
				// Normals
				#if defined( ECS_DETAILNORMALMAP ) && ( ECS_SHADER_QUALITY == 1 )
					#if defined( ECS_DETAIL_MASKMAP ) || defined( ECS_VERTEX_A_DETAILMASK )
						#if defined( ECS_NORMALMAP )
							normalTangent = ECS_BlendNormals( normalTangent, ECS_UnpackScaleNormal( UNITY_SAMPLE_TEX2D( _DetailBumpMap, detailUV ).rgba, _DetailBumpStrength * detailMask ) );
						#else
							half3 normalTangent = ECS_UnpackScaleNormal( UNITY_SAMPLE_TEX2D( _DetailBumpMap, detailUV ).rgba, _DetailBumpStrength * detailMask );
						#endif
					#else
						#if defined( ECS_NORMALMAP )
							#if ( ECS_SHADER_QUALITY == 1 )
								normalTangent = ECS_BlendNormals( normalTangent, ECS_UnpackScaleNormal( UNITY_SAMPLE_TEX2D( _DetailBumpMap, detailUV ).rgba, _DetailBumpStrength ) );
							#else
								normalTangent = ECS_BlendNormals( normalTangent, ECS_UnpackNormal( UNITY_SAMPLE_TEX2D( _DetailBumpMap, detailUV ).rgba ) );
							#endif
						#else
							#if ( ECS_SHADER_QUALITY == 1 )
								half3 normalTangent = ECS_UnpackScaleNormal( UNITY_SAMPLE_TEX2D( _DetailBumpMap, detailUV ).rgba, _DetailBumpStrength );
							#else
								half3 normalTangent = ECS_UnpackNormal( UNITY_SAMPLE_TEX2D( _DetailBumpMap, detailUV ).rgba );
							#endif
						#endif
					#endif
				#endif
			#endif

		//--SURFACE PROPERTIES ( SHADER SPECIFIC )--------------------------------------------

			//--LEGACY

				#if defined( ECS_SHADER_LEGACY )
					// Gloss Texture
					#if defined( ECS_GLOSSMAP )
						#if defined( ECS_ALPHATEST ) || defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
							fragment.gloss = UNITY_SAMPLE_TEX2D( _GlossMap, vertexOutput.UV12.xy ).r;
						#else
							fragment.gloss = diffuse.a;
						#endif
					// Gloss Slider
					#else
						fragment.gloss = _Gloss;
					#endif
	
					// Gloss Detail
					#if defined( ECS_DETAILMAP ) 
						#if defined( ECS_DETAIL_MASKMAP ) || defined( ECS_VERTEX_A_DETAILMASK )
							#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
								fragment.gloss = saturate( fragment.gloss + ( ( ( detail.g - 1.0h ) * _DetailRoughnessStrength ) + ( ( detail2.g - 1.0h ) * _Detail2RoughnessStrength ) ) * detailMask );
							#else
								fragment.gloss = saturate( fragment.gloss + ( detail.g - 1.0h ) * _DetailRoughnessStrength * detailMask );
							#endif
						#else
							#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
								fragment.gloss = saturate( fragment.gloss + ( ( detail.g - 1.0h ) * _DetailRoughnessStrength ) + ( ( detail2.g - 1.0h ) * _Detail2RoughnessStrength ) );
							#else
								fragment.gloss = saturate( fragment.gloss + ( detail.g - 1.0h ) * _DetailRoughnessStrength );
							#endif
						#endif
					#endif
	
					// Roughness
					#if defined( ECS_INDIRECTSPECULAR )
						fragment.roughness = 1.0h - fragment.gloss;
					#endif
	
					// Specular
					#if defined( ECS_PASS_DEFERRED )
						fragment.specular = ECS_INDIRECTSPECULAR_DIELECTRIC;
					#endif
	
					// Premultiplied Alpha
					#if defined( ECS_ALPHABLEND )		
						fragment.diffuse *= fragment.transparency;
					#endif	
				#endif
			
			//--PBR

				#if defined( ECS_SHADER_PBR )
					// Surface Texture		
					#if defined( ECS_SURFACEMAP )
						half3 physicalProperties = UNITY_SAMPLE_TEX2D( _SurfaceMap, vertexOutput.UV12.xy ).rgb;
						fragment.occlusion *= physicalProperties.b;
					// Surface Sliders
					#else
						half2 physicalProperties = half2( _Metalness, _Roughness );
					#endif
	
					// Roughness/Metalness Detail
					#if defined( ECS_DETAILMAP ) || ( defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 ) )
						#if defined( ECS_DETAIL_MASKMAP ) || defined( ECS_VERTEX_A_DETAILMASK )
							#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
								physicalProperties.rg = saturate( half2( physicalProperties.r + ( ( detail.b - 1.0h ) + ( detail2.b - 1.0h ) ) * detailMask, physicalProperties.g + ( ( ( detail.g - 1.0h ) * _DetailRoughnessStrength ) + ( ( detail2.g - 1.0h ) * _Detail2RoughnessStrength ) ) * detailMask ) );
							#else
								physicalProperties.rg = saturate( half2( physicalProperties.r + ( detail.b - 1.0h ) * detailMask, physicalProperties.g + ( detail.g - 1.0h ) * _DetailRoughnessStrength * detailMask ) );
							#endif
						#else
							#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
								physicalProperties.rg = saturate( half2( physicalProperties.r + ( detail.b - 1.0h ) + ( detail2.b - 1.0h ), physicalProperties.g + ( ( detail.g - 1.0h ) * _DetailRoughnessStrength ) + ( ( detail2.g - 1.0h ) * _Detail2RoughnessStrength ) ) );
							#else
								physicalProperties.rg = saturate( half2( physicalProperties.r + ( detail.b - 1.0h ), physicalProperties.g + ( detail.g - 1.0h ) * _DetailRoughnessStrength ) );
							#endif
						#endif
					#endif
	
					half OneMinusMetalness = 1.0h - physicalProperties.r;
	
					#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
						fragment.specular = lerp( ECS_INDIRECTSPECULAR_DIELECTRIC, fragment.diffuse, physicalProperties.r );	
					#else
						fragment.specular = lerp( unity_ColorSpaceDielectricSpec.rgb, fragment.diffuse, physicalProperties.r );	
					#endif
	
					fragment.diffuse = fragment.diffuse * OneMinusMetalness;
					fragment.roughness = physicalProperties.g;

					#if defined( ECS_ALPHABLEND )
						fragment.transparency = physicalProperties.r + OneMinusMetalness * fragment.transparency;
						fragment.diffuse *= fragment.transparency;
					#endif	
				#endif

		// Normals ( World Space )
		
		#if defined( ECS_INPUT_NORMAL )

			#if defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )

				#if defined( ECS_DIRLIGHTMAP ) && ( ECS_SHADER_QUALITY == 1 )
					fragment.normalWorld = ECS_NormalizePerPixel( vertexOutput.tangentToWorld1.xyz * fragment.normalTangent.x + vertexOutput.tangentToWorld2.xyz * fragment.normalTangent.y + vertexOutput.tangentToWorld3.xyz * fragment.normalTangent.z );
				#else
					fragment.normalWorld = ECS_NormalizePerPixel( vertexOutput.tangentToWorld1.xyz * normalTangent.x + vertexOutput.tangentToWorld2.xyz * normalTangent.y + vertexOutput.tangentToWorld3.xyz * normalTangent.z );
				#endif

			#else
				fragment.normalWorld = ECS_NormalizePerPixel( vertexOutput.normalWorld );
			#endif

		#endif

		//--VECTORS-------------------------------------------------------------------

			#if defined( ECS_INPUT_LIGHTING_DIRECT ) || defined( ECS_INPUT_LIGHTING_INDIRECT )
				#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD )
					fragment.eyeDirection = ECS_NormalizePerPixel(  vertexOutput.fogCoord.yzw  );
				#elif defined( ECS_PASS_DEFERRED )
					fragment.eyeDirection = ECS_NormalizePerPixel(  vertexOutput.eyeDirection  );
				#endif

					fragment.dotNV = ECS_DotClamped(  fragment.normalWorld, -fragment.eyeDirection  );
			#else
				#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD )
					half3 eyeDirection = ECS_NormalizePerPixel(  vertexOutput.fogCoord.yzw  );
					half dotNV = ECS_DotClamped(  fragment.normalWorld, -eyeDirection  );
				#elif defined( ECS_PASS_DEFERRED )
					half3 eyeDirection = ECS_NormalizePerPixel(  vertexOutput.eyeDirection  );
					half dotNV = ECS_DotClamped(  fragment.normalWorld, -eyeDirection  );
				#endif
			#endif

		//--TRANSPARENCY--------------------------------------------------------------------

			#if defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
				#if ( ECS_SHADER_QUALITY == 1 ) && ( defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED ) )
					#if defined( ECS_INPUT_LIGHTING_DIRECT ) || defined( ECS_INPUT_LIGHTING_INDIRECT )
						fragment.transparency *= UNITY_SAMPLE_TEX2D(  _CutoffFalloffRamp, half2(  fragment.dotNV, 0  )  ).r;
					#else
						fragment.transparency *= UNITY_SAMPLE_TEX2D(  _CutoffFalloffRamp, half2(  dotNV, 0  )  ).r;
					#endif
				#endif
			#endif

		// Emission

		#if defined( ECS_INPUT_EMISSION )
			fragment.emission = 0;

			#if !defined( ECS_EMISSION_HIGHLIGHT_ONLY )

				// Color

				fragment.emission = _EmissionColor;

				// Texture

				#if defined( ECS_EMISSIONMAP )
					
					// Colorspace Correction

					#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
						half3 emissionMap = UNITY_SAMPLE_TEX2D(_EmissionMap, vertexOutput.UV12.xy).rgb;

						if( ECS_IsGammaSpace(  ) == true )
							emissionMap = ECS_GammaToLinear( emissionMap );

						fragment.emission *= emissionMap;
					#else
						fragment.emission *= UNITY_SAMPLE_TEX2D( _EmissionMap, vertexOutput.UV12.xy ).rgb;
					#endif

				#endif

				// Vertex Color RGB

				#if defined( ECS_VERTEX_RGB_EMISSIONCOLOR )	
					fragment.emission *= vertexOutput.color.rgb;
				#endif
					
				// Vertex Color Alpha Mask

				#if defined( ECS_VERTEX_A_EMISSIONMASK )

					#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
						fragment.emission *= vertexOutput.color.a;
					#else
						fragment.emission *= vertexOutput.color;
					#endif

				#endif

				fragment.emission *= _EmissionStrength;
			#endif

			// Highlighting

			#if ( ECS_SHADER_QUALITY == 1 ) && ( defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED ) )

				#if defined( ECS_INPUT_LIGHTING_DIRECT ) || defined( ECS_INPUT_LIGHTING_INDIRECT )
					_HighlightColor *= UNITY_SAMPLE_TEX2D( _HighlightRamp, half2( fragment.dotNV,0 ) ).r;
				#else
					_HighlightColor *= UNITY_SAMPLE_TEX2D( _HighlightRamp, half2( dotNV,0 ) ).r;
				#endif

			#endif

			fragment.emission += _HighlightColor.rgb;
		#endif

		//--OCCLUSION---------------------------------------------------------------
		// Strength

			#if defined( ECS_INPUT_LIGHTING_OCCLUSION )
				#if ( ECS_SHADER_QUALITY == 1 )
					fragment.occlusion = pow( fragment.occlusion + ECS_EPSILON, _OcclusionStrength );
				#endif
			#endif

		//--INDIRECT SPECULAR---------------------------------------------------------------
		// Planar Reflection
		
			#if defined( ECS_INDIRECTSPECULAR_PLANAR ) && ( ECS_SHADER_QUALITY == 1 )
				#if defined( ECS_NORMALMAP ) || ( defined( ECS_DETAILNORMALMAP ) && ( ECS_SHADER_QUALITY == 1 ) )
					vertexOutput.posScreen.xy += normalTangent.xy;
				#endif
					fragment.planarReflection = tex2Dproj( _PlanarReflection, UNITY_PROJ_COORD( vertexOutput.posScreen ) );
			#endif
	}
#endif