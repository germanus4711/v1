using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Main : MonoBehaviour
    {
        private Canvas _viewCanvas;
        private GameObject _viewCanvasParent;

        private GameObject _gameObject;

        private Font ECSFont;

        private void Awake()
        {
        }

        public void Start()
        {
            ECSFont = (Font) Resources.Load("ARIALN");

            _gameObject = new GameObject
            {
                name = "MainCanvas"
            };
            _gameObject.AddComponent<Canvas>();
            // _viewCanvasParent = GameObject.Find("Manager").transform.parent.gameObject;

            _viewCanvas = _gameObject.GetComponent<Canvas>();
            _viewCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            _gameObject.AddComponent<CanvasScaler>();
            _gameObject.AddComponent<GraphicRaycaster>();

            var imp = new Import();
            imp.Load();
            Remote.Debug.Remote.Log($"Main: Start() lines = {imp.items.Count}");
            for (var lineCount = 0; lineCount < imp.items.Count; lineCount++)
                MakeText($"text{lineCount}", lineCount, imp.items[lineCount]);
            // MakeButton($"text{lineCount}", lineCount, imp.items[lineCount]);
            Remote.Debug.Remote.Log("Main: Start() DONE!");
        }

        private void MakeText(string name, int position, string impItem)
        {
            // Text
            var _text = new GameObject
            {
                transform =
                {
                    parent = _gameObject.transform
                },
                name = name
            };

            var _actualText = _text.AddComponent<Text>();
            _actualText.font = (Font) Resources.Load("ARIALN");
            _actualText.text = impItem;
            _actualText.fontSize = 32;
            _actualText.resizeTextForBestFit = true;

            // Text position
            var _rectTransform = _actualText.GetComponent<RectTransform>();
            _rectTransform.localPosition = new Vector3(0, 0 - _actualText.fontSize * position, 0);
            _rectTransform.sizeDelta = new Vector2(400, _actualText.fontSize);
        }

        private void MakeButton(string name, int position, string impItem)
        {
            var _button = new GameObject
            {
                transform =
                {
                    parent = _gameObject.transform
                },
                name = "b_" + name
            };

            // Text
            var _text = new GameObject
            {
                transform =
                {
                    parent = _gameObject.transform
                },
                name = name
            };

            var _actualText = _text.AddComponent<Text>();
            _actualText.font = ECSFont;
            _actualText.text = impItem;
            _actualText.fontSize = 32;
            _actualText.resizeTextForBestFit = true;

            // Text position
            var _rectTransform = _actualText.GetComponent<RectTransform>();
            _rectTransform.localPosition = new Vector3(0, 0, 0);
            _rectTransform.sizeDelta = new Vector2(400, _actualText.fontSize);

            var _actualButton = _button.AddComponent<Button>();
            var _buttonImage = _button.GetComponent<Image>();
            var _minRect = new Rect(0, 0, Texture2D.whiteTexture.width, Texture2D.whiteTexture.height);
            var sprite = Sprite.Create(Texture2D.whiteTexture, _minRect, Vector2.up);
            _buttonImage.sprite = sprite;

            // Button position
            var _rectTransformButton = _actualButton.GetComponent<RectTransform>();
            _rectTransformButton.localPosition = new Vector3(0, 0 - _actualText.fontSize * position, 0);
            _rectTransformButton.sizeDelta = new Vector2(400, _actualText.fontSize);
        }
    }
}