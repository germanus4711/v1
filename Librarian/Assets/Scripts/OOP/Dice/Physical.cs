#region Header
// ------------------------------------------------------------------------------------------
// 
//   File Name: Physical.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 22
//   Last Update: 2021 / 11 / 22
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------
#endregion

namespace OOP.Dice
{
    public class Physical
    {
        class Box
        {
            private double length; // Length of a box
            private double breadth; // Breadth of a box
            private double height; // Height of a box

            public double getVolume()
            {
                return length * breadth * height;
            }

            public void setLength(double len)
            {
                length = len;
            }

            public void setBreadth(double bre)
            {
                breadth = bre;
            }

            public void setHeight(double hei)
            {
                height = hei;
            }
        }
    }
}