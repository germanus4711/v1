//------------------------------------------------------------------------------------------
//
//  File Name:         	VertexOutput.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	February 28, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_VERTEXOUTPUT_CGINC )
	#define ECS_VERTEXOUTPUT_CGINC

	struct VertexOutput
	{
		//--POSITION------------------------------------------------------------------------
		
			float4 pos : SV_POSITION;
				
			#if defined( ECS_VERTEXOUTPUT_WORLDPOSITION )
				float3 posWorld : TEXCOORD8;
			#endif	
			
			#if defined( ECS_VERTEXOUTPUT_SCREENPOSITION )
				float4 posScreen : TEXCOORD9;
			#endif

		//--UV COORDINATES------------------------------------------------------------------
		
			// UV1, UV2
			#if defined( ECS_VERTEXOUTPUT_UV1 )
				#if defined( ECS_VERTEXOUTPUT_UV2 )
					float4 UV12 : TEXCOORD0;
				#else
					float2 UV12 : TEXCOORD0;
				#endif
			#elif defined( ECS_VERTEXOUTPUT_UV2 )
				float2 UV12 : TEXCOORD0;
			#endif
		
			// UV3, UV4
			#if defined( ECS_VERTEXOUTPUT_UV3 )
				#if defined( ECS_VERTEXOUTPUT_UV4 )
					float4 UV34 : TEXCOORD1;
				#else
					float2 UV34 : TEXCOORD1;
				#endif
			#elif defined( ECS_VERTEXOUTPUT_UV4 )
				float2 UV34 : TEXCOORD1;
			#endif
			
		//--NORMALS-------------------------------------------------------------------------

			#if defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
				// Also store indirect diffuse (forward base, deferred) or light direction (forward add)
				#if defined( ECS_VERTEXOUTPUT_INDIRECTDIFFUSE ) || ( defined( ECS_INPUT_LIGHTING_DIRECT ) && defined( ECS_PASS_FORWARDADD ) )
					half4 tangentToWorld1 : TEXCOORD2;
					half4 tangentToWorld2 : TEXCOORD3;
					half4 tangentToWorld3 : TEXCOORD4;
				#else
					half3 tangentToWorld1 : TEXCOORD2;
					half3 tangentToWorld2 : TEXCOORD3;
					half3 tangentToWorld3 : TEXCOORD4;
				#endif
			#else
				half3 normalWorld : TEXCOORD2;
			#endif
		
		//--VERTEX COLORS-------------------------------------------------------------------
			
			#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
				#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
					half4 color : COLOR;
				#else
					half3 color : COLOR;
				#endif
			#else
				#if defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
					half color : COLOR;
				#endif
			#endif
			
		//--LIGHTING------------------------------------------------------------------------

			//--INDIRECT

				// When normal mapping stored with tangent to world matrix
				#if !defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD ) && defined( ECS_VERTEXOUTPUT_INDIRECTDIFFUSE )
					half3 indirectDiffuse : TEXCOORD3;
				#endif

			//--DIRECT
			
				// Forward Base
				#if defined( ECS_INPUT_LIGHTING_DIRECT ) && defined( ECS_PASS_FORWARDBASE )
					// Light Direction - World Space
					half3 lightDirection : TEXCOORD6;

					// Shadow Receiving
					#if !defined( ECS_ALPHABLEND ) && !defined( ECS_ALPHAFADE )
						SHADOW_COORDS( 7 )
					#endif
				#endif
				
				// Forward Add
				#if defined( ECS_INPUT_LIGHTING_DIRECT ) && defined( ECS_PASS_FORWARDADD )
					// Light Direction - World Space
					// When normal mapping stored with tangent to world matrix
					#if !defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
						half3 lightDirection : TEXCOORD3;
					#endif

					// Shadow Receiving
					#if !defined( ECS_ALPHABLEND ) && !defined( ECS_ALPHAFADE )
						LIGHTING_COORDS( 6,7 )
					#endif
				#endif
				
		//--FOG/EYE DIRECTION---------------------------------------------------------------
		
			#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD )
				// Packed Fog Coordinates - X: Fog Coordinates, YZW: World Space Eye Direction
				UNITY_FOG_COORDS_PACKED( 5, half4 )
			#elif defined( ECS_PASS_DEFERRED )
				half3 eyeDirection: TEXCOORD5;
			#endif

		#if UNITY_VERSION >= 550
			UNITY_VERTEX_OUTPUT_STEREO
			UNITY_VERTEX_INPUT_INSTANCE_ID
		#endif
	};
#endif