//------------------------------------------------------------------------------------------
//
//  File Name:         	IndirectSpecular.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_LIGHTING_INDIRECT_SPECULAR_CGINC ) && defined( ECS_INPUT_LIGHTING_INDIRECT ) && defined( ECS_INDIRECTSPECULAR )
	#define ECS_LIGHTING_INDIRECT_SPECULAR_CGINC

	inline half3 ECS_Lighting_Indirect_Specular_BoxProjection ( half3 worldReflectionVector, float3 worldPosition, float4 cubemapCenter, float4 boxMin, float4 boxMax )
	{
		UNITY_BRANCH
		if ( cubemapCenter.w > 0 )
		{				
			half3 rbmax = ( boxMax.xyz - worldPosition ) / worldReflectionVector;
			half3 rbmin = ( boxMin.xyz - worldPosition ) / worldReflectionVector;
			half3 rbminmax = ( worldReflectionVector > 0 ) ? rbmax : rbmin;
			half fa = min( min( rbminmax.x, rbminmax.y ), rbminmax.z );
			worldPosition -= cubemapCenter.xyz;
			worldReflectionVector = worldPosition + worldReflectionVector * fa;
		}
		return worldReflectionVector;
	}			
		
	inline half3 ECS_Lighting_Indirect_Specular_CubeMap( UNITY_ARGS_TEXCUBE( tex ), half4 hdr, half3 worldReflectionVector, half roughness )
	{
		float mip = roughness * ECS_INDIRECTSPECULAR_LOD_STEPS;
		half4 rgbm = UNITY_SAMPLE_TEXCUBE_LOD( tex, worldReflectionVector.xyz, mip );
		return ECS_DecodeRGBM( rgbm, hdr );
	}

	#if defined( ECS_INDIRECTSPECULAR_PROBE )
		inline half3 ECS_Lighting_Indirect_Specular_Probe( half3 worldReflectionVector, VertexOutput vertexOutput, half roughness )
		{
			// Quality : High
			#if ( ECS_SHADER_QUALITY == 1 )
				half3 r0 = ECS_Lighting_Indirect_Specular_BoxProjection( worldReflectionVector, vertexOutput.posWorld, unity_SpecCube0_ProbePosition, unity_SpecCube0_BoxMin, unity_SpecCube0_BoxMax );
				half3 indirectSpecularCube = ECS_Lighting_Indirect_Specular_CubeMap( UNITY_PASS_TEXCUBE( unity_SpecCube0 ), unity_SpecCube0_HDR, r0, roughness );
			// Quality : Low ( Fallback )
			#else
				half3 indirectSpecularCube = ECS_Lighting_Indirect_Specular_CubeMap( UNITY_PASS_TEXCUBE( unity_SpecCube0 ), unity_SpecCube0_HDR, worldReflectionVector, roughness );
			#endif
	
			#if defined( ECS_INDIRECTSPECULAR_PROBE_BLENDING ) && ( ECS_SHADER_QUALITY == 1 )
				UNITY_BRANCH
				if ( unity_SpecCube0_BoxMin.w < kBlendFactor )
				{
					#if defined( ECS_INDIRECTSPECULAR_PROBE_BOXPROJECTION )
						half3 r1 = ECS_Lighting_Indirect_Specular_BoxProjection( worldReflectionVector, vertexOutput.posWorld, unity_SpecCube1_ProbePosition, unity_SpecCube1_BoxMin, unity_SpecCube1_BoxMax );
						half3 indirectSpecularCube1 = ECS_Lighting_Indirect_Specular_CubeMap( UNITY_PASS_TEXCUBE_SAMPLER( unity_SpecCube1,unity_SpecCube0 ), unity_SpecCube1_HDR, r1, roughness );
					#else
						half3 indirectSpecularCube1 = ECS_Lighting_Indirect_Specular_CubeMap( UNITY_PASS_TEXCUBE_SAMPLER( unity_SpecCube1,unity_SpecCube0 ), unity_SpecCube1_HDR, worldReflectionVector, roughness );
					#endif
					indirectSpecularCube = lerp( indirectSpecularCube1, indirectSpecularCube, unity_SpecCube0_BoxMin.w );
				}
			#endif
	
			return indirectSpecularCube;
		}
	#endif

	inline half3 ECS_Lighting_Indirect_Specular( half3 indirectSpecular, VertexOutput vertexOutput, Fragment fragment, half3 worldReflectionVector, half3 indirectSpecularBRDF )
	{
		// Unity Reflection Probe
		#if defined( ECS_INDIRECTSPECULAR_PROBE )
			indirectSpecular = ECS_Lighting_Indirect_Specular_Probe( worldReflectionVector, vertexOutput, fragment.roughness );
		// Custom Cubemap
		#elif defined( ECS_INDIRECTSPECULAR_CUSTOMCUBE )
			indirectSpecular = ECS_Lighting_Indirect_Specular_CubeMap( UNITY_PASS_TEXCUBE( _IndirectSpecularCube ), _IndirectSpecularCube_HDR, worldReflectionVector, fragment.roughness );
		// Planar Reflection
		#elif defined( ECS_INDIRECTSPECULAR_PLANAR ) && ( ECS_SHADER_QUALITY == 1 )
			indirectSpecular = fragment.planarReflection;
		#endif
		
		// Colorspace Correction
		#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )		
			if ( ECS_IsGammaSpace(  ) )
				indirectSpecular = ECS_GammaToLinear( indirectSpecular );
		#endif
				
		indirectSpecular *= indirectSpecularBRDF;

		// Occlusion
		#if defined( ECS_INPUT_LIGHTING_OCCLUSION )
			indirectSpecular *= fragment.occlusion.a;
		#endif

		return indirectSpecular;
	}
#endif