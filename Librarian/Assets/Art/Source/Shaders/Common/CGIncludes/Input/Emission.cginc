//------------------------------------------------------------------------------------------
//
//  File Name:         	Emission.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	May 8, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_EMISSION ) && ( defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_DEFERRED ) || defined( ECS_PASS_META ) )
	#define ECS_INPUT_EMISSION

	// Highlighting
	
	uniform half4 _HighlightColor;

	#if ( ECS_SHADER_QUALITY == 1 )
		uniform UNITY_DECLARE_TEX2D( _HighlightRamp );
	#endif

	//--EMISSION----------------------------------------------------------------------------

		#if !defined( ECS_EMISSION_HIGHLIGHT_ONLY )
			
			// Strength
			uniform half _EmissionStrength;

			// Color
			uniform half3 _EmissionColor;
	
			// Texture

			#if defined( ECS_EMISSIONMAP )

				#if !defined( ECS_VERTEXINPUT_UV1 )
					#define ECS_VERTEXINPUT_UV1
				#endif
				
				#if !defined( ECS_VERTEXOUTPUT_UV1 )
					#define ECS_VERTEXOUTPUT_UV1
				#endif
	
				uniform UNITY_DECLARE_TEX2D( _EmissionMap );
			#endif

			// Vertex Color RGB
			#if defined( ECS_VERTEX_RGB_EMISSIONCOLOR )

				#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
					#define ECS_VERTEXINPUT_VERTEXCOLORS
				#endif	
	
				#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB )
					#define ECS_VERTEXOUTPUT_VERTEXCOLORS_RGB
				#endif
	
				#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS_GAMMA )
					#define ECS_VERTEXINPUT_VERTEXCOLORS_GAMMA
				#endif

			#endif

		#endif

	//--EMISSION MASK-----------------------------------------------------------------------

		// Vertex Color Alpha
		#if !defined( ECS_EMISSION_HIGHLIGHT_ONLY )
			#if defined( ECS_VERTEX_A_EMISSIONMASK )
				#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
					#define ECS_VERTEXINPUT_VERTEXCOLORS
				#endif

				#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
					#define ECS_VERTEXOUTPUT_VERTEXCOLORS_A
				#endif
			#endif
		#endif
#endif