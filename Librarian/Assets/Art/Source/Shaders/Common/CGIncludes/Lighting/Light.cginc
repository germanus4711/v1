//------------------------------------------------------------------------------------------
//
//  File Name:         	Light.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	May 31, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_LIGHT_CGINC )
	#define ECS_LIGHT_CGINC
	
	struct Light
	{
		half3 color;
		half3 direction;
	};

	#if defined( ECS_INPUT_LIGHTING_DIRECT )
		inline void ECS_Light_Setup ( inout Light light, VertexOutput vertexOutput )
		{	
			//--COLOR

				light.color = _LightColor0.rgb;

				// Colorspace Correction
				#if defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER )
					if ( ECS_IsGammaSpace( ) )
						light.color = ECS_GammaToLinear( light.color );
				#endif
			
				#if defined( ECS_PASS_FORWARDBASE )
					#if !defined( ECS_ALPHABLEND ) && !defined( ECS_ALPHAFADE )
						light.color *= SHADOW_ATTENUATION( vertexOutput );
					#endif
				#elif defined( ECS_PASS_FORWARDADD )
					#if !defined( ECS_ALPHABLEND ) && !defined( ECS_ALPHAFADE )
						light.color *= LIGHT_ATTENUATION( vertexOutput );
					#endif
				#endif
			
			//--DIRECTION

				#if defined( ECS_PASS_FORWARDBASE )
					light.direction = ECS_NormalizePerPixel( vertexOutput.lightDirection.xyz );
				#elif defined( ECS_PASS_FORWARDADD )
					#if defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
						light.direction = ECS_NormalizePerPixel( half3( vertexOutput.tangentToWorld1.w, vertexOutput.tangentToWorld2.w, vertexOutput.tangentToWorld3.w ) );
					#else
						light.direction = ECS_NormalizePerPixel( vertexOutput.lightDirection.xyz );
					#endif
				#endif
		}
	#endif
#endif