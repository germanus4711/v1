using System.IO;
using UnityEngine;

namespace UI
{
    public class Helper
    {
        public static Texture2D LoadTexture(string filePath)
        {
            Texture2D tex2D;
            byte[] fileData;

            if (!File.Exists(filePath)) return null;

            fileData = File.ReadAllBytes(filePath);
            tex2D = new Texture2D(2, 2);

            return tex2D.LoadImage(fileData) ? tex2D : null;
        }

        public static Sprite LoadNewSprite(string filePath, float pixelsPerUnit = 100.0f,
            SpriteMeshType spriteType = SpriteMeshType.Tight)
        {
            var spriteTexture = LoadTexture(filePath);
            var newSprite = Sprite.Create(spriteTexture, new Rect(0, 0, spriteTexture.width, spriteTexture.height),
                new Vector2(0, 0), pixelsPerUnit, 0, spriteType);

            return newSprite;
        }

        public static Sprite ConvertTextureToSprite(Texture2D texture, float PixelsPerUnit = 100.0f,
            SpriteMeshType spriteType = SpriteMeshType.Tight)
        {
            // Converts a Texture2D to a sprite, assign this texture to a new sprite and return its reference

            var NewSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0),
                PixelsPerUnit, 0, spriteType);

            return NewSprite;
        }
    }
}