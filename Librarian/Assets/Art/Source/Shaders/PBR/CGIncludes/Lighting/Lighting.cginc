//------------------------------------------------------------------------------------------
//
//  File Name:         	Lighting.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 15, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_PBR_LIGHTING_CGINC )
	#define ECS_PBR_LIGHTING_CGINC

	#include "../Common/CGIncludes/Lighting/IndirectDiffuse.cginc"
	#include "../Common/CGIncludes/Lighting/IndirectSpecular.cginc"

	//--PBR STANDARD

			#define ECS_LIGHTING_FUNCTION ECS_Lighting_PBR( vertexOutput, fragment, light )

			inline half3 ECS_Lighting_PBR( VertexOutput vertexOutput, Fragment fragment, Light light )
			{
				half3 lighting = 0;
			
				//--VECTORS

					// Quality : High
					#if ( ECS_SHADER_QUALITY == 1 )
						// Direct Lighting
						#if defined( ECS_INPUT_LIGHTING_DIRECT )
							half dotNL = ECS_DotClamped( fragment.normalWorld, light.direction );
							half3 h = normalize( -fragment.eyeDirection + light.direction );	
							half dotNH = ECS_DotClamped( fragment.normalWorld, h );
							half dotLH = ECS_DotClamped( light.direction, h );
						#endif
						
						// Indirect Lighting
						#if defined( ECS_INPUT_LIGHTING_INDIRECT )
							#if defined( ECS_INDIRECTSPECULAR )	
								#if !defined( ECS_INDIRECTSPECULAR_PLANAR )
									half3 reflectionWorld = 2.0h * fragment.dotNV * fragment.normalWorld + fragment.eyeDirection;
								#else
									half3 reflectionWorld = 0;
								#endif
							#endif
						#endif
					// Quality : Low ( Fallback )
					#else
						// Direct Lighting
						#if defined( ECS_INPUT_LIGHTING_DIRECT )
							half dotNL = ECS_DotClamped( fragment.normalWorld, light.direction );
							half3 reflectionWorld = 2.0h * fragment.dotNV * fragment.normalWorld + fragment.eyeDirection;
							half2 rlPow4AndFresnelTerm = ECS_Pow4( half2( dot( reflectionWorld, light.direction ), 1.0h - fragment.dotNV ) );
						#endif
						
						// Indirect Lighting
						#if defined( ECS_INPUT_LIGHTING_INDIRECT )
							#if defined( ECS_INDIRECTSPECULAR )
								#if !defined( ECS_INPUT_LIGHTING_DIRECT )								
									#if !defined( ECS_INDIRECTSPECULAR_PLANAR )
										half3 reflectionWorld = 2.0h * fragment.dotNV * fragment.normalWorld + fragment.eyeDirection;
									#else
										half3 reflectionWorld = 0;
									#endif
								#else
									#if defined( ECS_INDIRECTSPECULAR_PLANAR )
										reflectionWorld = 0;
									#endif
								#endif
							#endif
						#endif
					#endif

				//--INDIRECT LIGHTING
		
					#if defined( ECS_INPUT_LIGHTING_INDIRECT )

						//--DIFFUSE

							half3 indirectDiffuse = 0;
							indirectDiffuse = ECS_Lighting_Indirect_Diffuse( indirectDiffuse, vertexOutput, fragment );
						
						//--SPECULAR

							half3 indirectSpecular = 0;
	
							#if defined( ECS_INDIRECTSPECULAR )
								// Quality : High
								#if ( ECS_SHADER_QUALITY == 1 )
									// Brian Karis, "Physically Based Shading On Mobile", 2014
									// https://www.unrealengine.com/blog/physically-based-shading-on-mobile
									// Used by Epic
									half4 c0 = half4( -1.0h, -0.0275h, -0.572h, 0.022h );
									half4 c1 = half4( 1.0h, 0.0425h, 1.04h, -0.04h );
									half4 r = fragment.roughness * c0 + c1;
									half a004 = min( r.x * r.x, exp2( -9.28h * fragment.dotNV ) ) * r.x + r.y;
									half2 aB = half2( -1.04h, 1.04h ) * a004 + r.zw;
									half3 indirectSpecularBRDF = fragment.specular * aB.x + aB.yyy;
								// Quality : Low ( Fallback )
								#else
									half grazingTerm = saturate( 1.0h - fragment.roughness + fragment.specular );
									half3 indirectSpecularBRDF = lerp( fragment.specular, grazingTerm, rlPow4AndFresnelTerm.y );
								#endif

								indirectSpecular = ECS_Lighting_Indirect_Specular( indirectSpecular, vertexOutput, fragment, reflectionWorld, indirectSpecularBRDF );
							#endif

						//--COMPOSITE

							// Debug
							#if defined( ECS_DEBUG_LIGHTING_INDIRECT_DIFFUSE )
								lighting = indirectDiffuse;
								return lighting;
							#elif defined( ECS_DEBUG_LIGHTING_INDIRECT_SPECULAR )
								lighting = indirectSpecular;
								return lighting;
							#elif defined( ECS_DEBUG_LIGHTING_INDIRECT )
								lighting = indirectDiffuse + indirectSpecular;
								return lighting;
							// Normal
							#else
								lighting = indirectDiffuse + indirectSpecular;
							#endif
					#endif

				//--DIRECT LIGHTING
					
					#if defined( ECS_INPUT_LIGHTING_DIRECT )

						//--DIFFUSE
						// Lambert

						//--SPECULAR
							
							// Quality : High
							#if ( ECS_SHADER_QUALITY == 1 )
								// Roughness Remapping
								half alpha = fragment.roughness * fragment.roughness; 
								half alpha2 = alpha * alpha;
								half directSpecularPower = ( dotNH * dotNH ) * ( alpha2 - 1.0h ) + 1.0h;
								directSpecularPower = alpha2 / ( 4.0h * directSpecularPower * directSpecularPower * dotLH * dotLH * ( fragment.roughness + 0.5h ) + ECS_EPSILON );
								half3 directSpecular = directSpecularPower * fragment.specular;
							// Quality : Low ( Fallback )
							#elif ( ECS_SHADER_QUALITY == 0 )
								half3 directSpecular = tex2D( unity_NHxRoughness, half2( rlPow4AndFresnelTerm.x, fragment.roughness ) ).UNITY_ATTEN_CHANNEL * 16.0h * fragment.specular;
							#endif

						//--COMPOSITE
						
							// Debug
							#if defined( ECS_DEBUG_LIGHTING_DIRECT_DIFFUSE )
								lighting = light.color * dotNL;
							#elif defined( ECS_DEBUG_LIGHTING_DIRECT_SPECULAR )
								lighting =  light.color * directSpecular * dotNL;
							#elif defined( ECS_DEBUG_LIGHTING_DIRECT )
								lighting = light.color * ( 1.0h + directSpecular ) * dotNL;
							#elif defined( ECS_DEBUG_LIGHTING )
								lighting += ( light.color * ( 1.0h + directSpecular ) * dotNL );
							// Normal
							#else
								lighting += ( light.color * ( fragment.diffuse + directSpecular ) * dotNL );
							#endif
					#endif

				//--OUTPUT

					return lighting;
			}
	
#endif