using RestSharp;

namespace Remote.Server.Rest
{
    public static class Get
    {
        public static string GetReleases(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest("", DataFormat.None);

            var retstr = client.Get(request);
            return retstr.Content;
        }
    }
}