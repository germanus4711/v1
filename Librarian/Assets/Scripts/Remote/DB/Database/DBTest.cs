﻿using Remote.DB.Database.data;
using UnityEngine;

namespace Remote.DB.Database
{
    public class DBTest : MonoBehaviour
    {
        private V1_Blacklist _blacklist;
        void Start()
        {
            _blacklist = new V1_Blacklist(); 
            _blacklist.FillList();

            foreach (var listed in _blacklist.Users)
            {
                UnityEngine.Debug.Log($">> {listed.UserId}");
            }
        }
    }
}
