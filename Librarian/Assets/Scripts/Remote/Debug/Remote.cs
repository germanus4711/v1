using System;
using RestSharp;

namespace Remote.Debug
{
    public static class Remote
    {
        private static Uri logServer = new Uri("http://3.93.150.141:8080");

        public static void Log(string s)
        {
            var client = new RestClient(logServer);
            var request = new RestRequest($"/Lumber/api/record?log={s}", Method.GET)
                .AddHeader("Accept", "text/plain, text/html")
                .AddHeader("content-type", "text/plain");

            var retstr = client.Get(request);
            UnityEngine.Debug.Log(retstr.Content);
        }
    }
}