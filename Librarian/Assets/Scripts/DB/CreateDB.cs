using System;
using System.Data;
using System.Data.Linq;
using System.Linq;
using dbo;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.Assertions;

namespace DB
{
    public class CreateDB : MonoBehaviour
    {
        // The name of the db.
        private readonly string _dbName = Globals.Localdb;

        // Start is called before the first frame update
        void Start()
        {
            Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, this,
                "<color=blue>Starting to create Database!</color>");
            try
            {
                using var connection = new SqliteConnection(_dbName);
                connection.Open();
                Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, this,
                    "<color=#ff0000ff>Connecting to Database!</color>");

                // Create an sql command that creates a new table.
                using var command = connection.CreateCommand();
            
                Assert.AreEqual(typeof(SqliteConnection), command.Connection.GetType());


                // // Drop the table if we call this method earlier.
                // command.CommandText = "DROP TABLE IF EXISTS Highscore;";
                // command.ExecuteNonQuery();

                command.CommandText = "CREATE TABLE IF NOT EXISTS Highscore (name VARCHAR(20), score INT);";
                command.ExecuteNonQuery();

                // Create test datasets
                command.CommandText = "INSERT INTO Highscore (name, score) VALUES ('André', 9000);";
                command.ExecuteNonQuery();

                command.CommandText = @"insert into Highscore (name, score) values ('William', 1233);
                                        insert into Highscore (name, score) values ('Sarah', 9055);";
                command.ExecuteNonQuery();

                // Read the datasets
                command.CommandText = "select * from Highscore order by score desc;";
                DBReader(command);

                
                command.Dispose();

                var context = new DataContext(connection);

                var highscores = context.GetTable<Highscore>();
                foreach (var player in highscores)
                {
                    Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, this,
                        "<color=fuchsia>Name = {0}</color>",
                        player.Name
                    );
                }

                var query = from sc in highscores where sc.Score == 15000 select sc;
                foreach (var hs in query)
                {
                    Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, this,
                        "<color=silver>Score (15000) = {0}</color>",
                        hs.Name
                    );
                }


                var hsquery = context.GetTable<Highscore>().Where(h => h.Name == "Sarah").Select(h => h.Score);
                foreach (var score in hsquery)
                {
                    Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, this,
                        "<color=aqua>Score (Cody) = {0}</color>",
                        score.ToString()
                    );
                }

                context.Dispose();

                connection.Close();

                // CRUD function tests

                var highscore = new Highscore(_dbName) {Name = "William", Score = 7000};
                highscore.Insert();

                highscore = new Highscore() {Name = "André", Score = -10};
                highscore.Update();

                highscore = new Highscore {Name = "William"};
                var hsl = highscore.Read();

                Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, this,
                    "William's score = {0}", hsl.Find(res => res.Name == "William").Score
                );
            }
            catch (Exception ex)
            {
                Debug.LogFormat(LogType.Error, LogOption.NoStacktrace, this, "DB Error: {0}", ex);
            }
        }

        private void DBReader(SqliteCommand command)
        {
            using IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                LogQuery(reader);
            }

            reader.Close();
        }

        private void LogQuery(IDataRecord reader)
        {
            Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, this,
                "<color=green>Name: {0} Score: <color=blue>{1}</color></color>",
                reader["name"], reader["score"]
            );
        }
    }
}
    