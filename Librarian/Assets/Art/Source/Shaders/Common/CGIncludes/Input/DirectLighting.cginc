//------------------------------------------------------------------------------------------
//
//  File Name:         	DirectLighting.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_LIGHTING_DIRECT ) && ( defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) )
	#define ECS_INPUT_LIGHTING_DIRECT
	
	uniform fixed4 _LightColor0;
	
	// TODO : Can this be removed?
	#if ( ECS_SHADER_QUALITY == 0 )
		uniform sampler2D unity_NHxRoughness;
	#endif
#endif