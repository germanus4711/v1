#region Header

// ------------------------------------------------------------------------------------------
// 
//   File Name: Environment.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 19
//   Last Update: 2021 / 11 / 19
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------

#endregion

public static class Environment
{
    public enum Environments
    {
        Production,
        Staging,
        Development
    }
}