#region Header

// ------------------------------------------------------------------------------------------
// 
//   File Name: V1_Migration.cs
//   Author: Andre Kruetzfeldt
//   Creation Date: 2021 / 11 / 19
//   Last Update: 2021 / 11 / 19
// 
// 
//   Copyrights: Copyright 2018 - 2021
//                       Engineering and Computer Simulations, Inc.
//                       All Rights Reserved.
// 
// -----------------------------------------------------------------------------------------

#endregion

using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using UnityEngine;
using Environment = Environment;

namespace Remote.DB.Database
{
    public class V1_Migration
    {
        public static string Database()
        {
            var EnvironmentName = Globals.EnvironmentName;
            // exclude db/datasets from production and staging environments
            string location = EnvironmentName != global::Environment.Environments.Production
                              || EnvironmentName != global::Environment.Environments.Staging
                ? Application.dataPath + "/Scripts/Remote/DB/Database/migrations" // only for development!
                : "db";
            UnityEngine.Debug.Log(">> " + location);
            try
            {
                var db = new V1_Database();
                db.DBSchema = "game";

                StringBuilder result = new StringBuilder();

                var cnx = new MySqlConnection(db.GetConnectionString());
                // for documemtation see: https://evolve-db.netlify.app
                var evolve = new Evolve.Evolve(cnx, msg => result.Append(msg + '\n'))
                {
                    Locations = new[] {location},
                    IsEraseDisabled = true,
                    Placeholders = new Dictionary<string, string>
                    {
                        ["${table4}"] = "table4"
                    }
                };

                evolve.Migrate();
                return result.ToString();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log("Database migration failed." + ex);
                throw;
            }
        }
    }
}