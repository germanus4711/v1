using System;
using System.Diagnostics;
using System.IO;
using UnityEngine;

namespace Launcher
{
    public class AppLauncher : MonoBehaviour
    {
        private Process _process;
        private StreamWriter _messageStream;

        public void Start()
        {
            StartProcess();
        }

        private void StartProcess()
        {
            try
            {
#if UNITY_EDITOR_OSX && !UNITY_IOS
                _process = new Process();
                _process.EnableRaisingEvents = false;
                _process.StartInfo.FileName =  @"/Applications/Opera.app/Contents/MacOS/Opera";
                #region Garbage!
                // _process.StartInfo.FileName = Application.dataPath + @"/path/to/The.app/Contents/MacOS/The";
                #endregion
                _process.StartInfo.Arguments = "--ran-launcher --remote https://ecsorl-cp.costpointfoundations.com/cpweb/cploginform.htm?1632751010";
                _process.StartInfo.UseShellExecute = false;
                _process.StartInfo.RedirectStandardOutput = true;
                _process.StartInfo.RedirectStandardInput = true;
                _process.StartInfo.RedirectStandardError = true;
                _process.OutputDataReceived += new DataReceivedEventHandler(DataReceived);
                _process.ErrorDataReceived += new DataReceivedEventHandler(ErrorReceived);
                _process.Start();
                _process.BeginOutputReadLine();
                _messageStream = _process.StandardInput;
#elif UNITY_IOS
                Application.OpenURL("https://ecsorl-cp.costpointfoundations.com/cpweb/cploginform.htm?1632751010");
#elif UNITY_ANDROID
                Application.OpenURL("https://ecsorl-cp.costpointfoundations.com/cpweb/cploginform.htm?1632751010");
#endif
                UnityEngine.Debug.Log("Successfully launched app");
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError("Unable to launch app: " + e.Message);
            }
        }


        private void DataReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            // Handle it
        }


        private void ErrorReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            UnityEngine.Debug.LogError(eventArgs.Data);
        }


        private void OnApplicationQuit()
        {
            if (_process is {HasExited: false} )
            {
                _process.Kill();
            }
        }
    }
}