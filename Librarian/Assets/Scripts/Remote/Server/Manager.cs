using UnityEngine;

namespace Remote.Server
{
    public class Manager : MonoBehaviour
    {
        private void Awake()
        {
        }

        // Start is called before the first frame update
        private void Start()
        {
            // read from Server
            var s = Rest.Get.GetReleases("http://3.80.157.125:8080/Lumber/api/record?log=Apple is here!");
            UnityEngine.Debug.Log(">> " + s);
        }

        // Update is called once per frame
        private void Update()
        {
        }
    }
}