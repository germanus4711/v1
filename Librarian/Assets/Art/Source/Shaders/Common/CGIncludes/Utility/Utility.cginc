//------------------------------------------------------------------------------------------
//
//  File Name:         	Utility.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_UTILITY_CGINC )
	#define ECS_UTILITY_CGINC
	
	//--MATH--------------------------------------------------------------------------------
	
		#define ECS_PI 3.1415926h
		#define ECS_EPSILON 1e-7h

		inline half ECS_Pow3( half x )
		{
			return x * x * x;
		}

		inline half2 ECS_Pow3( half2 x )
		{
			return x * x * x;
		}

		inline half3 ECS_Pow3( half3 x )
		{
			return x * x * x;
		}
	
		inline half ECS_Pow4( half x )
		{
			return x * x * x * x;
		}

		inline half2 ECS_Pow4( half2 x )
		{
			return x * x * x * x;
		}

		inline half3 ECS_Pow4( half3 x )
		{
			return x * x * x * x;
		}

		inline half ECS_Pow5( half x )
		{
			return x * x * x * x * x;
		}

		inline half2 ECS_Pow5( half2 x )
		{
			return x * x * x * x * x;
		}

		inline half3 ECS_Pow5( half3 x )
		{
			return x * x * x * x * x;
		}

		inline half ECS_LerpOneTo( half b, half t )
		{
			half oneMinusT = 1.0h - t;
			return oneMinusT + b * t;
		}

	//--VECTORS-----------------------------------------------------------------------------
	
		inline half3 ECS_NormalizePerVertex( half3 n ) 
		{
			#if ( ECS_SHADER_QUALITY == 1 )
				return n;
			#else
				return normalize( n );
			#endif
		}

		inline half3 ECS_NormalizePerPixel( half3 n )
		{
			#if ( ECS_SHADER_QUALITY == 1 )
				return normalize( n );
			#else
				return n;
			#endif
		}
	
		inline half ECS_DotClamped( half3 a, half3 b )
		{
			#if ( ECS_SHADER_QUALITY == 1 )
				return max( 0.0h, dot( a, b ) );
			#else
				return saturate( dot( a, b ) );
			#endif
		}
		
		inline half3 ECS_UnpackNormal( half4 packednormal )
		{
			#if defined( UNITY_NO_DXT5nm )
				return packednormal.xyz * 2.0h - 1.0h;
			#else
				half3 normal;
				normal.xy = ( packednormal.wy * 2.0h - 1.0h );
				normal.z = sqrt( 1.0h - saturate( dot( normal.xy, normal.xy ) ) );
				return normal;
			#endif
		}
		
		inline half3 ECS_UnpackScaleNormal( half4 packednormal, half bumpScale )
		{
			#if defined( UNITY_NO_DXT5nm )
				return packednormal.xyz * 2.0h - 1.0h;
			#else
				half3 normal;
				normal.xy = ( packednormal.wy * 2.0h - 1.0h );
				normal.xy *= bumpScale;
				normal.z = sqrt( 1.0h - saturate( dot( normal.xy, normal.xy ) ) );
				return normal;
			#endif
		}

		inline half3 ECS_BlendNormals( half3 normal1, half3 normal2 )
		{
			return normalize( half3( normal1.xy + normal2.xy, normal1.z * normal2.z ) );
		}

		inline half3x3 ECS_CreateTangentToWorldPerVertex( half3 normal, half3 tangent, half tangentSign )
		{
			// For odd-negative scale transforms we need to flip the sign
			tangentSign = tangentSign * unity_WorldTransformParams.w;
			half3 binormal = cross( normal, tangent ) * tangentSign;
			return half3x3( tangent, binormal, normal );
		}
	
	//--COLORS------------------------------------------------------------------------------

		inline bool ECS_IsGammaSpace( )
		{
			#if defined( UNITY_NO_LINEAR_COLORSPACE )
				return true;
			#else
				// Linear = 1, Gamma = 0
				return unity_ColorSpaceLuminance.w == 0;
			#endif
		}

		// Converts a color from linear space to gamma space with a fast approximation assuming gamma of 2.0 instead of 2.2
		inline half3 ECS_LinearToGamma( half3 color ) 
		{
			return sqrt( color );
		}
		
		// Converts a color from gamma space to linear space with a fast approximation assuming gamma of 2.0 instead of 2.2
		inline half3 ECS_GammaToLinear( half3 color ) 
		{
			return color * color;
		}
		
		// Converts a linear space color to it's percieved luminance value with a fast approximation
		inline half ECS_LinearToLuminance( half3 color ) 
		{
			#if ( ECS_SHADER_QUALITY == 0 )
				return color.g;
			#else
				return ( 0.299h * color.r + 0.587h * color.g + 0.114h * color.b );
			#endif
		}
		
		inline half3 ECS_LerpWhiteTo( half3 b, half t )
		{
			half oneMinusT = 1.0h - t;
			return half3( oneMinusT, oneMinusT, oneMinusT ) + b * t;
		}

		// Decode RGBM Textures
		inline half3 ECS_DecodeRGBM( half4 data, half4 decodeInstructions )
		{	
			#if !defined( ECS_COLORSPACE_GAMMA2LINEAR_SHADER ) && defined( UNITY_NO_LINEAR_COLORSPACE )
				return ( decodeInstructions.x * data.a ) * data.rgb;
			#else
				return ( decodeInstructions.x * pow( data.a, decodeInstructions.y ) ) * data.rgb;
			#endif
		}

	//--WIND----------------------------------------------------------------------------

		inline float3 ECS_VertexWind( float3 VertexPosition, float4 WindStrength, float4 DirectionalWindStrength, fixed Mask )
		{	
			#if ( UNITY_VERSION >= 550 )
				VertexPosition.xyz = mul( unity_ObjectToWorld, float4( VertexPosition, 1 ) ).xyz;
			#else
				VertexPosition.xyz = mul( _Object2World, float4( VertexPosition, 1 ) ).xyz;
			#endif

		    float3 wind = 0;
		    wind.x += sin( VertexPosition.x * ( 4.0f * DirectionalWindStrength.w ) +  _Time.y * ( WindStrength.w * 3.0f ) ) * ( WindStrength.x * 0.1f );
		    wind.x += cos( VertexPosition.x * ( 2.0f * DirectionalWindStrength.w ) +  _Time.y * WindStrength.w ) * ( WindStrength.x * 0.3f ) - DirectionalWindStrength.x;
		    wind.x += sin( VertexPosition.x * ( .02f * DirectionalWindStrength.w ) +  _Time.y * ( WindStrength.w * 0.5f ) ) * ( WindStrength.x * 0.4f ) - DirectionalWindStrength.x;
		    wind.y += sin( VertexPosition.y * ( 4.0f * DirectionalWindStrength.w ) +  _Time.y * ( WindStrength.w * 3.0f ) ) * ( WindStrength.y * 0.1f );
		    wind.y += cos( VertexPosition.y * ( 2.0f * DirectionalWindStrength.w ) +  _Time.y * WindStrength.w ) * ( WindStrength.y * 0.3f ) - DirectionalWindStrength.y;
		    wind.y += sin( VertexPosition.y * ( .02f * DirectionalWindStrength.w ) +  _Time.y * ( WindStrength.w * 0.5f ) ) * ( WindStrength.y * 0.4f ) - DirectionalWindStrength.y;
		   	wind.z += sin( VertexPosition.z * ( 4.0f * DirectionalWindStrength.w ) +  _Time.y * ( WindStrength.w * 3.0f ) ) * ( WindStrength.z * 0.1f );
		    wind.z += cos( VertexPosition.z * ( 2.0f * DirectionalWindStrength.w ) +  _Time.y * WindStrength.w ) * ( WindStrength.z * 0.3f ) - DirectionalWindStrength.z;
		    wind.z += sin( VertexPosition.z * ( .02f * DirectionalWindStrength.w ) +  _Time.y * ( WindStrength.w * 0.5f ) ) * ( WindStrength.z * 0.4f ) - DirectionalWindStrength.z;
		    wind *= Mask;
		   	VertexPosition.xyz += wind;

			#if ( UNITY_VERSION >= 550 )
				return mul( unity_WorldToObject, float4( VertexPosition, 1 ) ).xyz;
			#else
				return mul( _World2Object, float4( VertexPosition, 1 ) ).xyz;
			#endif
		}
#endif