//------------------------------------------------------------------------------------------
//
//  File Name:         	Config.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	February 28, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_PBR_CONFIG_CGINC )
	#define ECS_PBR_CONFIG_CGINC

	#define ECS_SHADER_PBR

	//--EARLY OUT FOR FORWARD ADD AND NO LIGHTING
	#if defined( ECS_PASS_FORWARDADD ) && !defined ( ECS_DIRECTLIGHTING )

		#include "../Common/CGIncludes/Pass/ForwardAddBypass.cginc"

	#else
	
		//--INCLUDES----------------------------------------------------------------------------

			// Unity
			#include "UnityCG.cginc"

			#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED )
				#include "AutoLight.cginc"
			#endif

			// ECS
			#include "../../../Common/CGIncludes/Config/Config.cginc"
			#include "../../../Common/CGIncludes/Utility/Utility.cginc"
		
		//--INPUTS------------------------------------------------------------------------------
	
			// Diffuse
			#include "../../../Common/CGIncludes/Input/Diffuse.cginc"

			// Normals
			#include "../../../Common/CGIncludes/Input/Normals.cginc"

			// Detail
			#include "../../..//Common/CGIncludes/Input/Detail.cginc"

			// Emission
			#include "../../../Common/CGIncludes/Input/Emission.cginc"

			// Lighting
			#if defined( ECS_DIRECTLIGHTING )
				#include "../Common/CGIncludes/Input/DirectLighting.cginc"
			#endif

			#include "../../../Common/CGIncludes/Input/IndirectLighting.cginc"
			#include "../../../Common/CGIncludes/Input/Occlusion.cginc"

			// Surface Properties (Shader Specific)
			#if !defined( ECS_PASS_SHADOWCASTING )
				#if defined( ECS_SURFACEMAP ) 
					#if !defined( ECS_VERTEXINPUT_UV1 )
						#define ECS_VERTEXINPUT_UV1
					#endif
				
					#if !defined( ECS_VERTEXOUTPUT_UV1 )
						#define ECS_VERTEXOUTPUT_UV1
					#endif
				
					#if !defined( ECS_INPUT_LIGHTING_OCCLUSION )
						#define ECS_INPUT_LIGHTING_OCCLUSION
						uniform fixed4 _OcclusionStrength;
					#endif
				
					UNITY_DECLARE_TEX2D( _SurfaceMap );
				#else
					uniform half _Roughness;
					uniform half _Metalness;
				#endif
			#endif

			// UV Transform
			#define ECS_INPUT_UV1_TRANSFORM
			#include "../../../Common/CGIncludes/Input/UV.cginc"

		//--PASSES------------------------------------------------------------------------------

			#if defined( ECS_PASS_META )
				#include "../Common/CGIncludes/Pass/Meta.cginc"
			#endif

			#if defined( ECS_PASS_SHADOWCASTING )
				#include "../Common/CGIncludes/Pass/Shadowcasting.cginc"
			#endif

			#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED )
				// Vertex Input
				#include "../Common/CGIncludes/Vertex/VertexInput.cginc"

				// Vertex Output
				#include "../Common/CGIncludes/Vertex/VertexOutput.cginc"

				// Vertex Program
				#include "../Common/CGIncludes/Vertex/VertexProgram.cginc"
			#endif

			#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED ) || defined( ECS_PASS_META )
				// Fragment Definition
				#include "../PBR/CGIncludes/Fragment/Fragment.cginc"
			#endif

			#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED )
				// Light Definition
				#include "../Common/CGIncludes/Lighting/Light.cginc"

				// Lighting 
				#include "../PBR/CGIncludes/Lighting/Lighting.cginc"
			#endif

			#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD ) || defined( ECS_PASS_DEFERRED ) || defined( ECS_PASS_META )
				// Fragment Output
				#include "../Common/CGIncludes/Fragment/FragmentOutput.cginc"

				// Fragment Program
				#include "../Common/CGIncludes/Fragment/FragmentProgram.cginc"
			#endif
	#endif
#endif