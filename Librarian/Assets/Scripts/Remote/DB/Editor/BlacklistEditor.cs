﻿using System;
using Remote.DB.Database.data;
using UnityEditor;
using UnityEngine;

namespace Remote.DB.Editor
{
    public class BlacklistEditor : EditorWindow
    {
        [MenuItem("Tools/Blacklister")]
        public static void OpenWindow()
        {
            BlacklistEditor window = (BlacklistEditor) GetWindow(typeof(BlacklistEditor));
            window.titleContent = new GUIContent("Player Blacklister");
            window.Show();
        }

        private enum FieldType
        {
            GUID,
            Email
        }

        private FieldType _fieldType = FieldType.GUID;
        private string _setVal = "";
        private string _error = null;
        private V1_Blacklist _blacklist;

        void OnGUI()
        {
            _blacklist = new V1_Blacklist();

            EditorGUILayout.LabelField("Player Blacklister", EditorStyles.boldLabel);
            EditorGUILayout.Separator();
            _fieldType = (FieldType) EditorGUILayout.EnumPopup("Selection Type", _fieldType);
            _setVal = EditorGUILayout.TextField("Player ", _setVal);

            if (_error != null)
            {
                EditorGUILayout.HelpBox(_error, MessageType.Error);
            }

            if (GUILayout.Button("Save"))
            {
                switch (_fieldType)
                {
                    case FieldType.GUID:
                    {
                        V1User user = new V1User {UserId = _setVal};
                        _blacklist.Save(user);
                        break;
                    }
                    case FieldType.Email:
                    {
                        _error = "Not implemented yet!";
                        return;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                _error = null;
            }

            if (GUILayout.Button("Remove"))
            {
                switch (_fieldType)
                {
                    case FieldType.GUID:
                    {
                        V1User user = new V1User {UserId = _setVal};
                        _blacklist.Remove(user);
                        break;
                    }
                    case FieldType.Email:
                    {
                        _error = "Not implemented yet!";
                        return;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                _error = null;
            }
        }
    }
}