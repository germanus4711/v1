//------------------------------------------------------------------------------------------
//
//  File Name:         	UV.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_WIND )
	#define ECS_INPUT_WIND

	// Vertex Color Alpha
	#if defined( ECS_VERTEX_A_WINDMASK ) && ( ECS_SHADER_QUALITY == 1 )
		#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
			#define ECS_VERTEXINPUT_VERTEXCOLORS
		#endif
		
		#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
			#define ECS_VERTEXOUTPUT_VERTEXCOLORS_A
		#endif

		uniform float4 _WindStrength;
		uniform float4 _DirectionalWindStrength;
	#endif
#endif