//------------------------------------------------------------------------------------------
//
//  File Name:         	Hair.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_HAIR )
	#define ECS_INPUT_HAIR
	
	#if !defined( ECS_VERTEXINPUT_UV1 )
		#define ECS_VERTEXINPUT_UV1
	#endif

	#if !defined( ECS_VERTEXOUTPUT_UV1 )
		#define ECS_VERTEXOUTPUT_UV1
	#endif

	uniform UNITY_DECLARE_TEX2D( _HairMap );
	uniform half _DetailStrength;

	uniform half _ShadowCutoff;
#endif