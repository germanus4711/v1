//------------------------------------------------------------------------------------------
//
//  File Name:         	Detail.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2018
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2018
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_INPUT_DETAIL ) && !defined( ECS_PASS_SHADOWCASTING )
	#define ECS_INPUT_DETAIL
	
	uniform half _DetailTiling;

	#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
		uniform half _Detail2Tiling;
	#endif

	//--DETAIL DIFFUSE/SURFACE--------------------------------------------------------------
	
		// Texture
		#if defined( ECS_DETAILMAP ) || ( defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 ) )
			#if defined( ECS_DETAIL_UV2 )
				#if !defined( ECS_VERTEXINPUT_UV2 )
					#define ECS_VERTEXINPUT_UV2
				#endif

				#if !defined( ECS_VERTEXOUTPUT_UV2 )
					#define ECS_VERTEXOUTPUT_UV2
				#endif
			#else
				#if !defined( ECS_VERTEXINPUT_UV1 )
					#define ECS_VERTEXINPUT_UV1
				#endif

				#if !defined( ECS_VERTEXOUTPUT_UV1 )
					#define ECS_VERTEXOUTPUT_UV1
				#endif
			#endif

			// PBR
			#if defined( ECS_SHADER_PBR )
				#if !defined( ECS_INPUT_LIGHTING_OCCLUSION )
					#define ECS_INPUT_LIGHTING_OCCLUSION
					uniform fixed4 _OcclusionStrength;
				#endif
			#endif

			uniform UNITY_DECLARE_TEX2D( _DetailMap );
			uniform half _DetailStrength;
			uniform half _DetailRoughnessStrength;

			#if defined( ECS_DETAILMAP2 ) && ( ECS_SHADER_QUALITY == 1 )
				uniform UNITY_DECLARE_TEX2D( _Detail2Map );
				uniform half _Detail2Strength;
				uniform half _Detail2RoughnessStrength;
			#endif
		#endif
		
	//--DETAIL NORMALS----------------------------------------------------------------------
	
		// Texture
		#if defined( ECS_DETAILNORMALMAP ) && ( ECS_SHADER_QUALITY == 1 )
			#if defined( ECS_DETAIL_UV2 )
				#if !defined( ECS_VERTEXINPUT_UV2 )
					#define ECS_VERTEXINPUT_UV2
				#endif

				#if !defined( ECS_VERTEXOUTPUT_UV2 )
					#define ECS_VERTEXOUTPUT_UV2
				#endif
			#else
				#if !defined( ECS_VERTEXINPUT_UV1 )
					#define ECS_VERTEXINPUT_UV1
				#endif

				#if !defined( ECS_VERTEXOUTPUT_UV1 )
					#define ECS_VERTEXOUTPUT_UV1
				#endif
			#endif

			#if !defined( ECS_VERTEXINPUT_TANGENT )
				#define ECS_VERTEXINPUT_TANGENT
			#endif
			
			#if !defined( ECS_VERTEXOUTPUT_TANGENTTOWORLD )
				#define ECS_VERTEXOUTPUT_TANGENTTOWORLD
			#endif

			uniform UNITY_DECLARE_TEX2D( _DetailBumpMap );
			uniform half _DetailBumpStrength;
		#endif
	
	//--DETAIL MASK--------------------------------------------------------------------------

		// Texture
		#if defined( ECS_DETAIL_MASKMAP )
			#if !defined( ECS_DETAIL_UV2 ) && !defined( ECS_VERTEXINPUT_UV1 )
				#define ECS_VERTEXINPUT_UV1
			#endif
			
			#if defined( ECS_DETAIL_UV2 ) && !defined( ECS_VERTEXINPUT_UV2 )
				#define ECS_VERTEXINPUT_UV2
			#endif

			uniform UNITY_DECLARE_TEX2D( _DetailMaskMap );
		#endif

		// Vertex Color Alpha
		#if defined( ECS_VERTEX_A_DETAILMASK )
			#if !defined( ECS_VERTEXINPUT_VERTEXCOLORS )
				#define ECS_VERTEXINPUT_VERTEXCOLORS
			#endif

			#if !defined( ECS_VERTEXOUTPUT_VERTEXCOLORS_A )
				#define ECS_VERTEXOUTPUT_VERTEXCOLORS_A
			#endif
		#endif
#endif