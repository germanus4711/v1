using System;
using Login;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public static class Make
    {
        public static void UIObjectInit(GameObject uiobject, GameObject uiparent)
        {
            uiobject.transform.position = new Vector3(0, 0, 0);
            uiobject.transform.localPosition = new Vector3(0, 0, 0);
            uiobject.transform.SetParent(uiparent.transform);
        }

        public static void ExpandRect(GameObject uielement, GameObject parent)
        {
            var panelRect = uielement.GetComponent<RectTransform>() == null
                ? uielement.AddComponent<RectTransform>()
                : uielement.GetComponent<RectTransform>();

            panelRect.anchoredPosition = new Vector2(0, 0);
            panelRect.anchorMin = new Vector2(0, 0);
            panelRect.anchorMax = new Vector2(1, 1);
            // panelRect.pivot = new Vector2(0.5f, 0.5f);
            panelRect.sizeDelta = parent is null
                ? new Vector2(0, 0)
                : parent.GetComponent<RectTransform>().sizeDelta;
        }
        
        public static GameObject Canvas(string canvasName, GameObject o)
        {
            var canvas = new GameObject
            {
                name = canvasName,
                layer = 5
            };
            canvas.AddComponent<CanvasScaler>();
            canvas.AddComponent<GraphicRaycaster>();

            var mainCanvas = canvas.GetComponent<Canvas>() == null
                ? canvas.AddComponent<Canvas>()
                : canvas.GetComponent<Canvas>();
            mainCanvas.renderMode = RenderMode.ScreenSpaceOverlay;


            
            return canvas;
        }

        public static GameObject Panel(GameObject root)
        {
            // Panels are GO + rect trans
            var panel = new GameObject
            {
                name = "Panel",
                layer = 5
            };
            panel.transform.SetParent(root.transform);
            panel.transform.position = new Vector3(0, 0, 0);
            panel.transform.localPosition = new Vector3(0, 0, 0);

            panel.AddComponent<CanvasRenderer>();
            var image = panel.AddComponent<Image>();

            var rect = panel.GetComponent<RectTransform>();
            rect.position = new Vector3(0, 0, 0);
            rect.localPosition = new Vector3(0, 0, 0);
            rect.offsetMax = Vector2.zero;
            rect.offsetMin = Vector2.zero;
            rect.sizeDelta = Vector2.zero;
            rect.anchoredPosition = Vector2.zero;
            rect.anchorMin = new Vector2(0, 0);
            rect.anchorMax = new Vector2(1, 1);
            // UnityEngine.Debug.Log($"{rect.offsetMax} , {rect.offsetMin}");
            return panel;
        }

        public static void Text(GameObject go, string verbiage)
        {
            var actualText = go.AddComponent<Text>();
            actualText.font = Config.Font;
            actualText.text = verbiage;
            actualText.fontSize = Config.FontSize;
            actualText.color = Color.blue;
            // Text position
            var rectTransform = actualText.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2(0, 0);
            rectTransform.position = new Vector3(0, 0, 0);
            rectTransform.sizeDelta = new Vector2(actualText.text.Length * actualText.fontSize * 1.2f,
                actualText.fontSize * 1.2f);
        }
        
        public static GameObject HorizontalGroup(GameObject group, string name = "HGroup")
        {
            var hzGroup = new GameObject
            {
                name = name,
                layer = 5
            };
            ExpandRect(hzGroup, group);
            hzGroup.AddComponent<HorizontalLayoutGroup>();
            UIObjectInit(hzGroup, group);

            var rect = hzGroup.GetComponent<RectTransform>();
            rect.transform.position = Vector3.zero;
            rect.transform.localPosition = Vector3.zero;
            return hzGroup;
        }
        
        private static GameObject VerticalGroup(GameObject uielement, string name = "VGroup")
        {
            var vGroup = new GameObject
            {
                name = name,
                layer = 5
            };
            ExpandRect(vGroup, uielement);
            vGroup.AddComponent<VerticalLayoutGroup>();

            UIObjectInit(vGroup, uielement);
            return vGroup;
        }

        public static GameObject Password(GameObject hzGroup, string text)
        {
            var go = new GameObject
            {
                name = "Password",
                layer = 5 // UI
            };
            var trans = go.AddComponent<RectTransform>();
            trans.anchoredPosition = new Vector2(0, 0);
            trans.position = new Vector3(0, 0, 0);

            var actualText = go.AddComponent<Text>();
            actualText.font = Config.Font;
            actualText.text = text;
            actualText.fontSize = Config.FontSize;
            actualText.color = Color.blue;
            // Text position
            var rectTransform = actualText.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2(0, 0);
            rectTransform.position = new Vector3(0, 0, 0);
            rectTransform.sizeDelta = new Vector2(actualText.text.Length * actualText.fontSize * 1.2f,
                actualText.fontSize * 1.2f);
            
            UIObjectInit(go, hzGroup);
            return go;
        }

        public static GameObject HUD()
        {
            EventSystem(); 
            
            var mainCanvas = Canvas("LoginCanvas", null);

            var panel = Panel(mainCanvas);
            panel.AddComponent<VerticalLayoutGroup>();
            return panel;
        }
        
        public static GameObject EventSystem()
        {
            var evgo = new GameObject
            {
                name = "EventSystem"
            };

            var mainEvent = evgo.GetComponent<EventSystem>() is null
                ? evgo.AddComponent<EventSystem>()
                : evgo.GetComponent<EventSystem>();
            
            var inputSystem = evgo.GetComponent<StandaloneInputModule>() is null
                ? evgo.AddComponent<StandaloneInputModule>()
                : evgo.GetComponent<StandaloneInputModule>();
            
            return evgo;
        }

        public static GameObject GetPassword(GameObject hzGroup, Action callback)
        {
            var go = new GameObject
            {
                name = "Passwd",
                layer = 5 // UI
            };
            var trans = go.AddComponent<RectTransform>();
            trans.anchoredPosition = new Vector2(0, 0);
            trans.position = new Vector3(0, 0, 0);

            var actualInputField = go.AddComponent<InputField>();
            actualInputField.characterLimit = 20;
            actualInputField.contentType = InputField.ContentType.Password;

            var placeholder = new GameObject
            {
                name = "Placeholder"
            };

            placeholder.AddComponent<Text>();
            placeholder.transform.SetParent(actualInputField.transform);
            var text = placeholder.GetComponent<Text>();
            text.font = Config.Font;
            text.text = "Put your password here";
            text.color = new Color(0, 0, 0, 128);
            actualInputField.placeholder = text;

            var texter = new GameObject
            {
                name = "Text"
            };
            texter.AddComponent<Text>();
            texter.transform.SetParent(actualInputField.transform);
            text = texter.GetComponent<Text>();
            text.font = Config.Font;
            text.text = string.Empty;
            text.color = Color.black;
            actualInputField.textComponent = texter.GetComponent<Text>();
            actualInputField.textComponent.font = Config.Font;
            actualInputField.textComponent.resizeTextForBestFit = true;
            actualInputField.onValueChanged.AddListener(delegate { callback(); });

            return go;
        }
        
        private static GameObject GetInput(GameObject hzGroup, Action callback)
        {
            var go = new GameObject
            {
                name = "Inout",
                layer = 5 // UI
            };
            var trans = go.AddComponent<RectTransform>();
            trans.anchoredPosition = new Vector2(0, 0);
            trans.position = new Vector3(0, 0, 0);

            var actualInputField = go.AddComponent<InputField>();
            actualInputField.characterLimit = 20;
            actualInputField.contentType = InputField.ContentType.Alphanumeric;
            actualInputField.onValueChanged.AddListener(delegate { callback(); });

            return go;
        }
    }
}