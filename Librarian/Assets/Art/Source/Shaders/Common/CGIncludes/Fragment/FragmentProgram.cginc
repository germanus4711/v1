//------------------------------------------------------------------------------------------
//
//  File Name:         	FragmentProgram.cginc
//  Author:            	Justin Welzien
//  Creation Date:  	March 24, 2015
//  Last Update:    	March 6, 2019
//                    	Justin Welzien
//
//  Copyrights:        	Copyright 2019
//                     	Engineering and Computer Simulations, Inc.
//                     	All Rights Reserved.
//
//------------------------------------------------------------------------------------------

// Prevent Unity From Auto-Upgrading Syntax ( Backwards Compatibility )
// UNITY_SHADER_NO_UPGRADE

#if !defined( ECS_FRAGMENTPROGRAM_CGINC )
	#define ECS_FRAGMENTPROGRAM_CGINC

	// Deferred
	#if defined( ECS_PASS_DEFERRED )
		void FragmentProgram
		( 
			VertexOutput vertexOutput,
			out half4 outDiffuse : SV_Target0,			// RT0: Diffuse ( RGB ), Occlusion ( A )
			out half4 outSpecSmoothness : SV_Target1,	// RT1: Specular Color ( RGB ), Gloss ( A )
			out half4 outNormal : SV_Target2,			// RT2: Normal ( RGB ), --UNUSED ( Low Precision )-- ( A ) 
			out half4 outEmission : SV_Target3			// RT3: Emission ( RGB ), --UNUSED-- ( A )
		 )
	// Meta
	#elif defined( ECS_PASS_META )
		float4 FragmentProgram( VertexOutput vertexOutput ) : SV_Target
	// Forward Base/Add
	#else
		half4 FragmentProgram( VertexOutput vertexOutput ) : SV_Target 
	#endif
	{
		// Fragment Setup
		Fragment fragment;
		UNITY_INITIALIZE_OUTPUT( Fragment, fragment );
		ECS_Fragment_Setup( fragment, vertexOutput );

		UNITY_SETUP_INSTANCE_ID( vertexOutput );
		UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( vertexOutput );
			
		// Meta Pass Output
		#if defined( ECS_PASS_META )
			UnityMetaInput meta;
			UNITY_INITIALIZE_OUTPUT( UnityMetaInput, meta );
			
			#if defined( ECS_SHADER_PBR )
				meta.Albedo = fragment.diffuse + ( fragment.specular * fragment.roughness * fragment.roughness * 0.5h );
			#else
				meta.Albedo = fragment.diffuse;
			#endif
			
			#if defined( ECS_INPUT_EMISSION )
				meta.Emission = fragment.emission;
			#else
				meta.Emission = 0;
			#endif

			return UnityMetaFragment( meta );
		#endif

		// Lighting Setup
		#if defined( ECS_INPUT_LIGHTING_DIRECT ) ||  defined( ECS_INPUT_LIGHTING_INDIRECT )
			Light light;
			UNITY_INITIALIZE_OUTPUT( Light, light );
			#if defined( ECS_INPUT_LIGHTING_DIRECT )
				ECS_Light_Setup( light, vertexOutput );
			#endif
			half3 fragmentColor = ECS_LIGHTING_FUNCTION;
		#else
			half3 fragmentColor = fragment.diffuse;
		#endif

		// Emission
		#if defined( ECS_INPUT_EMISSION ) && !( defined( ECS_DEBUG_LIGHTING ) || defined( ECS_DEBUG_LIGHTING_INDIRECT ) || defined( ECS_DEBUG_LIGHTING_INDIRECT_DIFFUSE ) || defined( ECS_DEBUG_LIGHTING_INDIRECT_SPECULAR ) || defined( ECS_DEBUG_LIGHTING_DIRECT ) || defined( ECS_DEBUG_LIGHTING_DIRECT_DIFFUSE ) || defined( ECS_DEBUG_LIGHTING_DIRECT_SPECULAR ) )
			fragmentColor += fragment.emission;
		#endif

		// Fog
		#if defined( ECS_PASS_FORWARDBASE )
			UNITY_APPLY_FOG( vertexOutput.fogCoord, fragmentColor.rgb );
		#elif defined( ECS_PASS_FORWARDADD )
			UNITY_APPLY_FOG_COLOR( vertexOutput.fogCoord, fragmentColor.rgb, half4( 0, 0, 0, 0 ) );
		#endif

		// Debug
		#if defined( ECS_DEBUG_DIFFUSE )
			fragmentColor = fragment.diffuse;
			#if defined( ECS_PASS_FORWARDADD )
				fragmentColor = 0;
			#endif
		#elif defined( ECS_DEBUG_GLOSS )
			fragmentColor = fragment.gloss;
			#if defined( ECS_PASS_FORWARDADD )
				fragmentColor = 0;
			#endif
		#elif defined( ECS_DEBUG_NORMALS )
			fragmentColor = fragment.normalWorld;
			#if defined( ECS_PASS_FORWARDADD )
				fragmentColor = 0;
			#endif
		#elif defined( ECS_DEBUG_EMISSION )
			fragmentColor = fragment.emission;
			#if defined( ECS_PASS_FORWARDADD )
				fragmentColor = 0;
			#endif
		#endif

		// Deferred Pass Output
		#if defined( ECS_PASS_DEFERRED )

			#if !defined( UNITY_HDR_ON )
				fragmentColor = exp2( -fragmentColor );
			#endif
			
			#if defined( ECS_INPUT_LIGHTING_OCCLUSION )
				outDiffuse = half4( fragment.diffuse, fragment.occlusion.g );
			#else	
				outDiffuse = half4( fragment.diffuse, 1.0h );
			#endif
			outSpecSmoothness = half4( fragment.specular, fragment.gloss );
			outNormal = half4( fragment.normalWorld * 0.5h + 0.5h , 1.0h );
			outEmission = half4( fragmentColor, 1.0h );
		#endif

		// Forward Base/Add Pass Output
		#if defined( ECS_PASS_FORWARDBASE ) || defined( ECS_PASS_FORWARDADD )
			// Colorspace Correction
			#if defined( ECS_COLORSPACE_LINEAR2GAMMA_SHADER )
				if ( ECS_IsGammaSpace(  ) )
					fragmentColor = ECS_LinearToGamma( fragmentColor );
			#endif
			
			#if defined( ECS_ALPHABLEND ) || defined( ECS_ALPHAFADE ) || defined( ECS_ALPHADECAL )
				return half4( fragmentColor, fragment.transparency );
			#else
				return half4( fragmentColor, 1.0h );
			#endif
		#endif
	}
#endif