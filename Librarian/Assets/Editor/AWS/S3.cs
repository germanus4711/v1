using Remote.AWS;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class S3 : MonoBehaviour
    {
        [MenuItem("AWS/S3/Get")]
        private static void DoSomething()
        {
            Debug.Log("Getting S3 ...");
            var b = new Bucket();
            b.ListAll();
        }
    }
}